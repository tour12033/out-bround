export default 
{
    "categories": [
        {
            "id": "1",
            "name": "Architecture department",
            "shortName": "AR"
        },
        {
            "id": "2",
            "name": "Interior Architecture department",
            "shortName": "IN"
        },
        {
            "id": "3",
            "name": "Interior Design department",
            "shortName": "IND"
        },
        {
            "id": "4",
            "name": "Product Design department",
            "shortName": "PD"
        }
    ]
}