export default 
{
    "sustainables": [
        {
            "id": "1",
            "name": "Sustainable"
        },
        {
            "id": "2",
            "name": "Innovation"
        },
        {
            "id": "3",
            "name": "Urban"
        },
        {
            "id": "4",
            "name": "Local"
        },
        {
            "id": "5",
            "name": "Heritage"
        },
        {
            "id": "6",
            "name": "Education"
        },
        {
            "id": "7",
            "name": "Entertainment"
        },
        {
            "id": "8",
            "name": "Commercial"
        },
        {
            "id": "9",
            "name": "Health & Wellness"
        },
        {
            "id": "10",
            "name": "Art & Culture"
        },
        {
            "id": "11",
            "name": "Consumption"
        },
        {
            "id": "12",
            "name": "Fashion"
        },
        {
            "id": "13",
            "name": "Social"
        }
    ]
}