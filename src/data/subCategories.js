export default 
{
    "subCategories": [
        {
            "id": "1",
            "name": "Museum",
            "categoryId": "1"
        },
        {
            "id": "2",
            "name": "Hub",
            "categoryId": "1"
        },
        {
            "id": "3",
            "name": "Hospitality",
            "categoryId": "1"
        },
        {
            "id": "4",
            "name": "Education",
            "categoryId": "1"
        },
        {
            "id": "5",
            "name": "Highrise",
            "categoryId": "1"
        },
        {
            "id": "6",
            "name": "Transportation Hub",
            "categoryId": "1"
        },
        {
            "id": "7",
            "name": "Resort",
            "categoryId": "1"
        },
        {
            "id": "8",
            "name": "Government",
            "categoryId": "1"
        },
        {
            "id": "9",
            "name": "Religious",
            "categoryId": "1"
        },
        {
            "id": "10",
            "name": "Museum",
            "categoryId": "2"
        },
        {
            "id": "11",
            "name": "Hub",
            "categoryId": "2"
        },
        {
            "id": "12",
            "name": "Hospitality",
            "categoryId": "2"
        },
        {
            "id": "13",
            "name": "Education",
            "categoryId": "2"
        },
        {
            "id": "14",
            "name": "Center",
            "categoryId": "2"
        },
        {
            "id": "15",
            "name": "Museum",
            "categoryId": "3"
        },
        {
            "id": "16",
            "name": "Hotel",
            "categoryId": "3"
        },
        {
            "id": "17",
            "name": "Hostel",
            "categoryId": "3"
        },
        {
            "id": "18",
            "name": "Gym & Nutrition Club",
            "categoryId": "3"
        },
        {
            "id": "19",
            "name": "Center",
            "categoryId": "3"
        },
        {
            "id": "20",
            "name": "Materials Science / Design and Development",
            "categoryId": "4"
        },
        {
            "id": "21",
            "name": "Design and Development / Art and Culture",
            "categoryId": "4"
        },
        {
            "id": "22",
            "name": "Design and Development",
            "categoryId": "4"
        },
        {
            "id": "23",
            "name": "Social Enterprises - Green Design",
            "categoryId": "4"
        },
        {
            "id": "24",
            "name": "Consumer Driven / Design and Development",
            "categoryId": "4"
        }
    ]
}