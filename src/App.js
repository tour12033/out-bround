import React, { Component } from "react";
import { connect } from 'react-redux'
import { Route, Switch, withRouter } from "react-router-dom";
import renderIf from 'render-if';
// import { Navbar, Nav, Form, FormControl, Button } from 'react-bootstrap';
import "./App.scss";

import Navbar from './Navbar';
const ComingSoonPage = React.lazy(() => import("./views/Pages/ComingSoonPage/ComingSoonPage"));
const HomePage = React.lazy(() => import("./views/Pages/HomePage/HomePage"));
const FormalPage = React.lazy(() => import("./views/Pages/FormalPage/FormalPage"));
const InteractivePage = React.lazy(() => import("./views/Pages/InteractivePage/InteractivePage"));
const ProjectsPage = React.lazy(() => import("./views/Pages/ProjectsPage/ProjectsPage"));
const DetailsPage = React.lazy(() => import("./views/Pages/DetailsPage/DetailsPage"));
const AboutPage = React.lazy(() => import("./views/Pages/AboutPage/AboutPage"));
const CategoryPage = React.lazy(() => import("./views/Pages/CategoryPage/CategoryPage"));
const NewsPage = React.lazy(() => import("./views/Pages/NewsPage/NewsPage"));
const ContactPage = React.lazy(() => import("./views/Pages/ContactPage/ContactPage"));
// const DefaultLayout = React.lazy(() => import("./containers/DefaultLayout"));

const loading = () => (
  // <div className="animated fadeIn pt-3 text-center">Loading...</div>
  <div className="animated fadeIn pt-3 text-center"></div>
);

class App extends Component {
  getLocation() {
    // let location = useLocation();
    return (this.props.location.pathname)
  }
  render() {

    return (
      <div>
        {/* {this.getLocation()} */}
        <Navbar />
        <div style={{ marginTop: 90 }}>
          <React.Suspense fallback={loading()}>
            <Switch>
              {/* <Route
              exact
              path="/exampleRedux"
              name="Example Redux"
              render={props => <ExampleRedux {...props} />}
            /> */}
              {/* <Route
              exact
              path="/"
              name="Coming soon Page"
              render={props => <ComingSoonPage {...props} />}
            />
            <Route
              exact
              // path="/"
              name="Coming soon Page"
              render={props => <ComingSoonPage {...props} />}
            /> */}
              <Route
                exact
                path="/"
                name="Home Page"
                render={props => <HomePage {...props} />}
              />
              <Route
                exact
                path="/Formal"
                name="Formal Page"
                render={props => <FormalPage {...props} />}
              />
              <Route
                exact
                path="/Interactive"
                name="Interactive Page"
                render={props => <InteractivePage {...props} />}
              />
              <Route
                exact
                path="/Projects/:id"
                name="Projects Page"
                render={props => <ProjectsPage {...props} />}
              />
              <Route
                exact
                path="/Details/:id"
                name="Details Page"
                render={props => <DetailsPage {...props} />}
              />

              <Route
                exact
                path="/About"
                name="About Page"
                render={props => <AboutPage {...props} />}
              />

              <Route
                exact
                path="/category/:id"
                name="Category Page"
                render={props => <CategoryPage {...props} />}
              />
              <Route
                exact
                path="/category"
                name="Category Page"
                render={props => <CategoryPage {...props} />}
              />

              {/* <Route
              path="/News"
              name="News Page"
              render={props => <NewsPage {...props} />}
            /> */}

              <Route
                exact
                path="/Contact"
                name="Contact Page"
                render={props => <ContactPage {...props} />}
              />
              {/*
            <Route
              exact
              path="/ThaiLottery"
              name="ThaiLottery"
              render={props => <ThaiLottery {...props} />}
            />
            <Route
              exact
              path="/ThaiLotteryConfirm"
              name="ThaiLotteryConfirm"
              render={props => <ThaiLotteryConfirm {...props} />}
            />
            <Route
              exact
              path="/ThaiLotteryHardConfirm"
              name="ThaiLotteryHardConfirm"
              render={props => <ThaiLotteryHardConfirm {...props} />}
            />
            <Route
              exact
              path="/register"
              name="Register Page"
              render={props => <Register {...props} />}
            />
            <Route
              exact
              path="/history"
              name="History Page"
              render={props => <History {...props} />}
            />
            <Route
              exact
              path="/HistoryDetail"
              name="History Page"
              render={props => <HistoryDetail {...props} />}
            />
            <Route
              exact
              path="/CheckLottery"
              name="Check Lottery Page"
              render={props => <CheckLottery {...props} />}
            /> */}
              {/* <Route
              path="/"
              name="Home"
              render={props =>
                fakeAuth.isAuthenticated === true ? ( //this.props.user.isLogin
                  <DefaultLayout {...props} />
                ) : (
                    <Login {...props} />
                  )
              }
            /> */}
            </Switch>
          </React.Suspense>
        </div>
        {/* {this.props.loading.isLoading ? (
          <LoadingPage />
        ) : null} */}
      </div>
    );
  }
}

// export default App;
const mapStateToProps = state => ({
  user: state.user,
  loading: state.loading
});

const mapDispatchToProps = dispatch => ({
  //   storeUserInfo: (firstName, lastName, phone, email, citizenId, status, token, permisions) => {
  //     dispatch({ 
  //       type: ActionUser.STORE_USER_INFO,
  //       firstName: firstName,
  //       lastName: lastName,
  //       phone: phone,
  //       email: email,
  //       citizenId: citizenId,
  //       status: status,
  //       token: token,
  //       permisions: permisions
  //   })
  // }
});
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(App));
