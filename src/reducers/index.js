import { combineReducers } from 'redux'
import counterReducer from './counters'
import userReducer from './user'
import loadingReducer from './loading'
import serviceReducer from './service'
import lotteryReducer from './lottery'

export default combineReducers({
  counters: counterReducer,
  user: userReducer,
  loading: loadingReducer,
  service: serviceReducer,
  lottery: lotteryReducer
})