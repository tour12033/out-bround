import actionLoading from "../actions/actionLoading";

var initialState = {
  isLoading: false
};

function loadingReducer(state = initialState, action) {
  switch (action.type) {
    case actionLoading.IS_LOADING:
      return {
        isLoading: true
      };
    case actionLoading.NO_LOADING:
      return {
        isLoading: false
      };
    default:
      return state;
  }
}

export default loadingReducer;
