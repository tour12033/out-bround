import ActionUser from "../actions/actionUser";

var initialState = {
  isLogin: false,
  id: null,
  firstName: null,
  lastName: null,
  phone: null,
  email: null,
  citizenId: null,
  status: null,
  accessToken: null,
  permisions: [],
  balance: 0
};

function userReducer(state = initialState, action) {
  switch (action.type) {
    case ActionUser.STORE_USER_INFO:
      return {
        isLogin: true,
        id: action.id,
        firstName: action.firstName,
        lastName: action.lastName,
        phone: action.phone,
        email: action.email,
        citizenId: action.citizenId,
        status: action.status,
        accessToken: action.accessToken,
        permisions: action.permisions
      };
    case ActionUser.STORE_USER_ACCESS_TOKEN:
      return {
        ...state,
        accessToken: action.accessToken
      };
    case ActionUser.RESET_LOGIN:
      return {
        initialState
      };
      case ActionUser.STORE_BALANCE:
        return {
          ...state,
          balance: action.balance
        };
    default:
      return state;
  }
}

export default userReducer;
