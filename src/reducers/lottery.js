import ActionLottery from '../actions/actionLottery'

var initialState = {
  isEasyMode: false,
  rateList: []
};

function lotteryReducer(state = initialState, action) {
  switch (action.type) {
    // case ActionService.STORE_SERVICE_INFO:
    //   return {

    //     id: action.id,
    //     image: action.image,
    //     name: action.name,
    //     roundId: action.roundId,
    //     service_endtime: action.service_endtime,
    //     service_starttime: action.service_starttime
    //   };
    case ActionLottery.SET_EZ_MODE:
        return {
          ...state,
          isEasyMode: true
        };
    case ActionLottery.SET_HARD_MODE:
        return {
          ...state,
          isEasyMode: false
        };
    case ActionLottery.STORE_DATA_EZ_MODE:
        return {
          ...state,
          rateList: action.data
        };
    case ActionLottery.RESET_DATA_EZ_MODE:
        return {
          ...state,
          rateList: []
        };
    default:
      return state;
  }
}

export default lotteryReducer;
