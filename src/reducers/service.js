import ActionService from '../actions/actionService'

var initialState = {
  id: null,
  image: null,
  name: null,
  roundId: null,
  service_endtime: null,
  service_starttime: null
};

function serviceReducer(state = initialState, action) {
  switch (action.type) {
    case ActionService.STORE_SERVICE_INFO:
      return {

        id: action.id,
        image: action.image,
        name: action.name,
        roundId: action.roundId,
        service_endtime: action.service_endtime,
        service_starttime: action.service_starttime
      };

    default:

      return state;
  }
}

export default serviceReducer;
