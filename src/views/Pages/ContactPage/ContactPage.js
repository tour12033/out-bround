import React, { Component } from "react";
import { Container, Row, Col, Alert, Card, Image } from 'react-bootstrap';
import Countdown from 'react-countdown';
import { Link } from "react-router-dom";
import { Modal } from 'react-bootstrap';
// import { Slide } from 'react-slideshow-image';
import renderIf from 'render-if';
import Swal from "sweetalert2";
import { connect } from "react-redux";

import ActionUser from "../../../actions/actionUser";
import ActionService from "../../../actions/actionService";
import ActionLottery from "../../../actions/actionLottery";

import './styles.css';

import projects from '../../../data/projects.js'
import images from '../../../data/images.js'
import profiles from '../../../data/profiles.js'
import subCategories from '../../../data/subCategories.js'
import categories from '../../../data/categories.js'

import facebook from "../../../assets/img/fb_contact.png";
import ig from "../../../assets/img/ig_contact.png";
import youtube from "../../../assets/img/yt_contact.png";
import facebook_white from "../../../assets/img/fb_contact_white.png";
import ig_white from "../../../assets/img/ig_contact_white.png";
import youtube_white from "../../../assets/img/yt_contact_white.png";
import web from "../../../assets/img/web.png";


class FormalPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      dataYeeGee: [],
      isShowModal: false,
      isSelectValue: false,
      isShowAlert: false,
      textAlert: '',
      valueAll: 0,
      roundId: '',
      onlineUser: 0,
      username: '',
      selectLotto: 1
    };
  }

  async componentDidMount() {
    // let data = await []
    // let imagesList = await []
    // let profile = null
    // let subCategory = null
    // let category = null
    // await projects.projects.forEach(projectItem => {
    //   imagesList = []
    //   profile = null
    //   subCategory = null
    //   images.images.forEach(imageItem => {
    //     if (projectItem.id === imageItem.projectId) {
    //       imagesList.push(imageItem)
    //     }
    //   });
    //   profile = profiles.profiles.find(profileItem => (projectItem.profileId === profileItem.id));
    //   subCategory = subCategories.subCategories.find(subCategoryItem => (subCategoryItem.id === projectItem.subCategoryId));
    //   category = categories.categories.find(categoryItem => (categoryItem.id === subCategory.categoryId))
    //   data.push({
    //     id: projectItem.id,
    //     projectName: projectItem.projectName,
    //     detailTitle: projectItem.detailTitle,
    //     detailFull: projectItem.detailFull,
    //     latitude: projectItem.latitude,
    //     longitude: projectItem.longitude,
    //     profile: profile,
    //     subCategory: subCategory,
    //     images: imagesList,
    //     CategoryId: category.id,
    //     CategoryName: category.name
    //   })
    // });
    // console.log(await data);
  }
  render() {
    return (
      <div>
      <Container style={{ backgroundColor: 'white', fontFamily: 'myriad-pro-condensed', paddingBottom: 18 }} >
        <Row style={{ paddingTop: 86 }}>
        <Col xs={12} md={6} lg={6} className='d-flex align-items-center justify-content-around' style={{ flexDirection: 'column' }}>
          <h1 style={{fontWeight: 'bold', fontStyle: 'italic'}}>
            CONTACT US
          </h1>
          </Col>
        </Row>
        <Row className='justify-content-around'>
          <Col xs={12} md={6} lg={6} className='d-flex align-items-center' style={{ flexDirection: 'column' }}>
            <div
              style={{ width: '18rem', cursor: 'pointer' }}
              onClick={() => window.open('https://www.facebook.com/arch.au.edu')}
            >
              <Card.Body style={{ color: '#192F58' }}>
                <Row className='align-items-center justify-content-around' style={{ marginBottom: 18 }}>
                  <Col xs={3} md={3} lg={3}>
                  <img
                      alt=""
                      src={facebook}
                      // width="30"
                      height="40"
                      className="d-inline-block align-top"
                    />
                  </Col>
                  <Col xs={9} md={9} lg={9}>
                    <Card.Text>
                      Montfort del Rosario School of Architecture and Design
                    </Card.Text>
                  </Col>
                </Row>
              </Card.Body>
            </div>
            <div
              style={{ width: '18rem', cursor: 'pointer' }}
              onClick={() => window.open('http://www.arch.au.edu')}
            >
              <Card.Body style={{ color: '#192F58' }}>
                <Row className='align-items-center justify-content-around' style={{ marginBottom: 18 }}>
                  <Col xs={3} md={3} lg={3}>
                    <img
                      alt=""
                      src={web}
                      // width="30"
                      height="40"
                      className="d-inline-block align-top"
                    />
                  </Col>
                  <Col xs={9} md={9} lg={9}>
                    <Card.Text>
                      www.arch.au.edu
                      </Card.Text>
                  </Col>
                </Row>
              </Card.Body>
            </div>
          </Col>
          <Col xs={12} md={6} lg={6} className='d-flex justify-content-center align-items-center' style={{ flexDirection: 'column' }}>
            <div className='cicleRed' style={{position: 'absolute'}}></div>
            <div style={{ backgroundColor: '#A51E26', borderRadius: 10 }}>
              <div
                style={{ width: '18rem', cursor: 'pointer' }}
                onClick={() => window.open('https://facebook.com/Thesisexhibition2020/?tsid=0.33230680231473797&source=result')}
              >
                <Card.Body style={{ color: 'white' }}>
                  <Row className='align-items-center justify-content-around' style={{ marginBottom: 18 }}>
                    <Col xs={3} md={3} lg={3}>
                      <img
                        alt=""
                        src={facebook_white}
                        // width="30"
                        height="40"
                        className="d-inline-block align-top"
                      />
                    </Col>
                    <Col xs={9} md={9} lg={9}>
                      <Card.Text>
                        Outbound: AAU Thesis
                        Exhibition 2020
                      </Card.Text>
                    </Col>
                  </Row>
                </Card.Body>
              </div>
              <div
                style={{ width: '18rem', cursor: 'pointer' }}
                onClick={() => window.open('https://www.instagram.com/outbound_2020/')}
              >
                <Card.Body style={{ color: 'white' }}>
                  <Row className='align-items-center justify-content-around' style={{ marginBottom: 18 }}>
                    <Col xs={3} md={3} lg={3}>
                      <img
                        alt=""
                        src={ig_white}
                        // width="30"
                        height="40"
                        className="d-inline-block align-top"
                      />
                    </Col>
                    <Col xs={9} md={9} lg={9}>
                      <Card.Text>
                        outbound_2020
                      </Card.Text>
                    </Col>
                  </Row>
                </Card.Body>
              </div>
              <div
                style={{ width: '18rem', cursor: 'pointer' }}
                onClick={() => window.open('https://www.youtube.com/channel/UCOf32iPFo2J2qZnW-ihL6YQ/featured')}
              >
                <Card.Body style={{ color: 'white' }}>
                  <Row className='align-items-center justify-content-around' style={{ marginBottom: 18 }}>
                    <Col xs={3} md={3} lg={3}>
                      <img
                        alt=""
                        src={youtube_white}
                        // width="30"
                        height="40"
                        className="d-inline-block align-top"
                      />
                    </Col>
                    <Col xs={9} md={9} lg={9}>
                      <Card.Text>
                        Out of Boundary 2020
                  </Card.Text>
                    </Col>
                  </Row>
                </Card.Body>
              </div>
            </div>
          </Col>
        </Row>
      </Container >
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user
});

const mapDispatchToProps = dispatch => ({
  storeUserAccessToken: (
    accessToken
  ) => {
    dispatch({
      type: ActionUser.STORE_USER_ACCESS_TOKEN,
      accessToken: accessToken
    });
  },
  storeUserBalance: (
    balance
  ) => {
    dispatch({
      type: ActionUser.STORE_BALANCE,
      balance: balance
    });
  },
  storeServiceInfo: (
    id,
    image,
    name,
    roundId,
    service_endtime,
    service_starttime
  ) => {
    dispatch({
      type: ActionService.STORE_SERVICE_INFO,
      id: id,
      image: image,
      name: name,
      roundId: roundId,
      service_endtime: service_endtime,
      service_starttime: service_starttime
    });
  },
  setHardMode: () => {
    dispatch({
      type: ActionLottery.SET_HARD_MODE
    });
  },
  resetDataEzMode: () => {
    dispatch({
      type: ActionLottery.RESET_DATA_EZ_MODE
    });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(FormalPage);
