import React, { Component } from "react";
import { Container, Row, Col, Alert } from 'react-bootstrap';
import Fade from 'react-reveal/Fade';
import Countdown from 'react-countdown';
import { Link } from "react-router-dom";
import { Modal } from 'react-bootstrap';
// import { Slide } from 'react-slideshow-image';
import renderIf from 'render-if';
import Swal from "sweetalert2";
import { connect } from "react-redux";
import YouTube from 'react-youtube';
import './styles.css';

class AboutPage extends Component {

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  async componentDidMount() {

  }




  render() {

    return (
      <Container style={{ backgroundColor: 'white', fontFamily: 'myriad-pro-condensed', paddingBottom: 32 }}>
        {/* <Row className='justify-content-center align-items-center' style={{ backgroundColor: 'red' }}>
          <p>
            <firstletter>While</firstletter>it is still unknown as to when the official reopening of fitness facilities will commence, this intervention has created a safe and innovative way to practice physical activity, support local businesses, provide jobs to instructors across the city, as well as support the mental health of the community.  
          </p>
        </Row> */}
        <Row className='justify-content-center' style={{ paddingTop: 18 }}>
          <h1 style={{ fontWeight: 'bold', fontStyle: 'italic', color: '#192F58' }}>ABOUT US</h1>
        </Row>
        <Row style={{ paddingTop: 18 }}>
          {/* youtube video */}
          <Col xs={12} md={8}>
            <YouTube
              videoId="6L1majfaz_8"
              opts={{
                height: '400',
                width: '100%',
                // padding: '20'
              }}
            />
          </Col>
          {/* end youtube video */}

          {/* promote video */}
          <Col xs={12} md={4} style={{ textAlign: 'center' }}>
            <Row className='d-flex justify-content-center'>

            </Row>
            <Row>
              <Col xs={12} md={12} lg={12} style={{ textAlign: 'center' }}>
                {/* Description of “Out Bound” <br /> */}
                Out of your boundary : The implication of this particular word defined into two term. The first boundary, the graduation from academic and start our new journey as a designer. The second boundary was due to an unexceptional condition of the covid-19, we come across a New limitation of new normal lifestyle opening new perceptions.
                  <br /><br />
                  Through the past year, we have learned that the scope of the architectural works does not limit to the design part. However, the architect is also responsible for various types of works such as research, analysis, construction planning, etc. All this aspects brought us out from our comfort zone which is the main reason for this thesis exhibition “Out of your boundary”
                  <br /><br />
                  Let’s explore the Architecture graduated student from Assumption University year 2019 that put heart and souls into their work. From the Architecture 5 years, Interior Architecture 5 years, Interior Design 4 years, and lastly Product Design 4 years.
              </Col>
            </Row>

          </Col>
          {/* end promote video */}
        </Row>

        <Row className="video-content">
          <Col sm={12} md={12} lg={12} style={{ marginTop: 32 }} ><h2>Teaser</h2></Col>
          {/* <Col sm={12} md={6} lg={3} >
            <Fade>
              <YouTube videoId="yb0j2zc9_JQ" onReady={this._onReady} className="youtube" />
              <h3 className="youtube-title">Special Guest</h3>
              <h6 className="youtube-title">สถาปัตย์ลาดกระบัง 20 ปีที่แล้ว</h6>
            </Fade>
          </Col> */}
          <Col sm={12} md={4} lg={4} style={{marginBottom: 12}} >
            <Fade>
              <YouTube videoId="nbWYYFYSjss" onReady={this._onReady} className="youtube" />
            </Fade>
          </Col>
          <Col sm={12} md={4} lg={4} style={{marginBottom: 12}} >
            <Fade>
              <YouTube videoId="jkpMIv8akog" onReady={this._onReady} className="youtube" />
            </Fade>
          </Col>
          <Col sm={12} md={4} lg={4}  style={{marginBottom: 12}}>
            <Fade>
              <YouTube videoId="OWU7ByFEPDc" onReady={this._onReady} className="youtube" />
            </Fade>
          </Col>
          <Col sm={12} md={12} lg={12} style={{ marginTop: 32 }}><h2>Influencer</h2></Col>
          <Col sm={12} md={6} lg={3} style={{marginBottom: 12}} >
            <Fade>
              <YouTube videoId="-wSO_TRC0vo" onReady={this._onReady} className="youtube" />
            </Fade>
          </Col>
          <Col sm={12} md={6} lg={3} style={{marginBottom: 12}} >
            <Fade>
              <YouTube videoId="FTPvzNuC140" onReady={this._onReady} className="youtube" />
            </Fade>
          </Col>
          <Col sm={12} md={6} lg={3} style={{marginBottom: 12}} >
            <Fade>
              <YouTube videoId="giRcZQ-Iotg" onReady={this._onReady} className="youtube" />
            </Fade>
          </Col>
          <Col sm={12} md={6} lg={3}  style={{marginBottom: 12}}>
            <Fade>
              <YouTube videoId="2gMgJ9IYUcM" onReady={this._onReady} className="youtube" />
            </Fade>
          </Col>
          <Col sm={12} md={12} lg={12} style={{ marginTop: 32 }}><h2>Interview</h2></Col>


          <Col sm={12} md={6} lg={3} style={{marginBottom: 12}}>
            <Fade>
              <YouTube videoId="4a1I4dL3uL8" onReady={this._onReady} className="youtube" />
            </Fade>
          </Col>

          <Col sm={12} md={6} lg={3} style={{marginBottom: 12}}>
            <Fade>
              <YouTube videoId="ESk6HMaYKWo" onReady={this._onReady} className="youtube" />
            </Fade>
          </Col>

          <Col sm={12} md={6} lg={3} style={{marginBottom: 12}}>
            <Fade>
              <YouTube videoId="TvyphlM979c" onReady={this._onReady} className="youtube" />
            </Fade>
          </Col>

          <Col sm={12} md={6} lg={3} style={{marginBottom: 12}}>
            <Fade>
              <YouTube videoId="2A_Jkc5h5eI" onReady={this._onReady} className="youtube" />
            </Fade>
          </Col>
        </Row>
      </Container >
    );
  }
}





export default connect()(AboutPage);
