import React, { Component } from "react";

import { connect } from 'react-redux'

import ActionCount from '../../../actions/actionCount'
import {
  Container,
  Button,
  Row
} from "reactstrap";


class ExampleRedux extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      submitted: false,
      count: 0
    };

  }

  handleChange = e => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  componentWillReceiveProps(nextProps) {
  }

  onAdd = e => {
    this.props.increment();

  };
  onDec = e => {
    this.props.decrement();

  };

  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">{this.props.counter.counter}</Row>
          <Row className="justify-content-center">
            <Button
              color="danger"
              className="px-4"
              onClick={this.onDec}
            >-</Button>
            <Button
              color="primary"
              className="px-4"
              onClick={this.onAdd}
            >+</Button>
            </Row>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  counter: state.counters
});

const mapDispatchToProps = dispatch => ({
  increment: () => dispatch({ type: ActionCount.INCREMENT, text: "INCREMENT Redux" }),
  decrement: () => dispatch({ type: ActionCount.DECREMENT, text: "DECREMENT Redux" }),
});

export default connect(mapStateToProps, mapDispatchToProps)(ExampleRedux);
