import React, { Component } from "react";
import { Row, Col, Image } from 'react-bootstrap';
import logo from "../../../assets/img/logo.png";
import { connect } from "react-redux";
import './styles.css';

class AboutPage extends Component {

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  async componentDidMount() {

  }
  render() {

    return (
      <Row className='justify-content-center align-items-center circle' style={{ height: '100vh', marginLeft: 0, marginRight: 0 }}>
        <Col sm={12} md={10} lg={8} className='d-flex justify-content-center align-items-center'>
          <Row className='justify-content-center align-items-center'>
            <Col sm={6} md={6} lg={6}>
              {/* <div> */}
              <Image
                fluid
                alt=""
                src={logo}
              />
            </Col>
            <Col sm={6} md={6} lg={6} className='d-flex justify-content-center'>
              <div style={{fontFamily: 'myriad-pro-condensed', fontWeight: 'bold', fontStyle: 'italic', color: '#1A2F59'}}>
                <h1>
                  OUT OF BOUNDARY
                </h1>
                <h1>
                  THESIS EXHIBITION
                </h1>
                <h1 style={{color: '#A51E26' }}>
                  COMING SOON
                </h1>
              </div>
            </Col>
          </Row>
        </Col>
        {/* </div> */}
      </Row>
    );
  }
}





export default connect()(AboutPage);
