import _ from "lodash";
import React, { Component } from "react";
import { Container, Row, Col, Alert, Form, Image } from 'react-bootstrap';
import { compose, withProps, withHandlers } from "recompose";
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
  InfoWindow
} from "react-google-maps";
import { MarkerClusterer } from "react-google-maps/lib/components/addons/MarkerClusterer";
import mark from "../../../assets/img/mark.png";

import projects from '../../../data/projects.js'
import images from '../../../data/images.js'
import subCategories from '../../../data/subCategories.js'
import sustainables from '../../../data/sustainables.js'
const mapStyles = require("./mapStyles.json")
// import GitHubForkRibbon from "react-github-fork-ribbon";
// import Header from "../../Header";
var iconPinRed = {
  path: 'M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z',
  fillColor: '#BB565D',
  fillOpacity: 1,
  scale: 0.04, //to reduce the size of icons
};
var iconPinBlue = {
  path: 'M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z',
  fillColor: '#536382',
  fillOpacity: 1,
  scale: 0.04, //to reduce the size of icons
};
var iconPinPink = {
  path: 'M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z',
  fillColor: '#F59BA1',
  fillOpacity: 1,
  scale: 0.04, //to reduce the size of icons
};
var iconPinLightBlue = {
  path: 'M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z',
  fillColor: '#7DA3BB',
  fillOpacity: 1,
  scale: 0.04, //to reduce the size of icons
};
const getPinColor = (marker) => {
  if (marker.categoryId === '1') {
    return iconPinRed
  } else if (marker.categoryId === '2') {
    return iconPinBlue
  } else if (marker.categoryId === '3') {
    return iconPinLightBlue
  } else {
    return iconPinPink
  }
};
const enhance = _.identity;
const MyMapComponent = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyAFpvbzMt_dquFEWpevH45afBPi_o60Eb0&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%`, width: '100%', borderRadius: 15 }} />,
    containerElement: <div style={{ height: `100%`, width: '100%', fontFamily: 'myriad-pro-condensed', borderRadius: 15 }} />,
    mapElement: <div style={{ height: `100%`, width: '100%', fontFamily: 'myriad-pro-condensed', borderRadius: 15 }} />,
  }),
  withScriptjs,
  withGoogleMap
)(props =>
  <GoogleMap
    defaultZoom={5}
    defaultCenter={{ lat: 13.736717, lng: 100.523186 }}
    defaultOptions={mapStyles}
    defaultClickableIcons={false}
  >
    <MarkerClusterer
      onClick={props.onMarkerClustererClick}
      averageCenter
      enableRetinaIcons
      gridSize={50}
      // imagePath={logo}
      styles={[
        {
          url: mark,
          textColor: 'white',
          height: 20,
          // lineHeight: 10,
          width: 20,
        }
      ]}
    >
      {props.markers.map(marker => (
        <Marker
          icon={getPinColor(marker)}
          styles={{ height: 10, width: 10 }}
          key={marker.id}
          // icon={logo}
          position={{ lat: Number(marker.latitude), lng: Number(marker.longitude) }}
          onClick={() => { props.onMarkerClick(marker.id) }}
        >
          {props.showId === marker.id && <InfoWindow onCloseClick={props.onCloseInfo}>
            <div style={{ fontFamily: 'myriad-pro-condensed', paddingRight: 20 }}>
              <div className='d-flex row justify-content-center align-items-center'>
                {marker.image ? <Image src={require('../../../data/project/' + marker.image)} height={200} width={'auto'} rounded /> : null}

                <div style={{ marginLeft: 12 }}>
                  <div style={{ marginBottom: 12, fontFamily: 'myriad-pro-condensed', fontWeight: 'bold', fontStyle: 'italic', fontSize: 24 }}>{marker.projectName}</div>
                  <div style={{ cursor: 'pointer', textDecoration: 'underline', fontFamily: 'myriad-pro-condensed', fontSize: 20 }} onClick={() => { props.goProjectDetail(marker.id) }}>Detail</div>
                </div>
              </div>
            </div>
          </InfoWindow>}
        </Marker>

      ))}
    </MarkerClusterer>
  </GoogleMap>
)

class MyFancyComponent extends React.PureComponent {
  state = {
    filter: [],
    selectFilter: [],
    isMarkerShown: false,
    markers: [],
    showId: ''
  }

  componentDidMount() {
    let listProject = projects.projects
    let listImage = images.images
    listProject.forEach((itemProject, indexProject) => {
      listImage.forEach((itemImage, indexImage) => {
        if (itemImage.projectId === itemProject.id && itemImage.isTitle === "TRUE") {
          if (itemImage.image) {
            listProject[indexProject].image = itemImage.image
          }
        }
      });
      if (itemProject.subCategoryId) {
        let subCategory = subCategories.subCategories.find(element => element.id === itemProject.subCategoryId.match(/\d+/)[0])
        if (subCategory) {
          listProject[indexProject].categoryId = subCategory.categoryId
        }
      }
    });
    this.delayedShowMarker()
    this.setState({
      markers: listProject,
      filter: sustainables.sustainables
    });
    // const url = [
    //   // Length issue
    //   `https://gist.githubusercontent.com`,
    //   `/farrrr/dfda7dd7fccfec5474d3`,
    //   `/raw/758852bbc1979f6c4522ab4e92d1c92cba8fb0dc/data.json`
    // ].join("")

    // fetch(url)
    //   .then(res => res.json())
    //   .then(data => {
    //     this.setState({ markers: data.photos });
    //   });
  }

  onMarkerClick = (id) => {
    // console.log(id);
    this.setState({
      showId: id
    })
  };

  onCloseInfo = () => {
    this.setState({
      showId: ''
    })
  };

  delayedShowMarker = () => {
    setTimeout(() => {
      this.setState({ isMarkerShown: true })
    }, 3000)
  }

  goProjectDetail = (id) => {
    window.location.href = "/Details/" + id;
  }

  handleMarkerClick = () => {
    this.setState({ isMarkerShown: false })
    this.delayedShowMarker()
  }

  handleChangeCheckBox(evt, id) {
    let list = this.state.selectFilter
    if (evt.target.checked) {
      if (!list.includes(id)) {
        list.push(id)
      }
    } else {
      const index = list.indexOf(id);
      if (index > -1) {
        list.splice(index, 1);
      }
    }
    this.setState({
      selectFilter: list
    })
    this.getDataFilter()
    // this.setState({ checkboxChecked: evt.target.checked });
  }
  getDataFilter() {
    let listProject = []
    this.state.selectFilter.forEach(filter => {
      projects.projects.forEach((itemProject, indexProject) => {
        let sustainableIdList = []
        if (itemProject.sustainableId) {
          sustainableIdList = itemProject.sustainableId.split(",");
          if (sustainableIdList.includes(filter)) {
            listProject.push(itemProject)
            if (listProject[indexProject]) {
              if (itemProject.subCategoryId) {
                let subCategory = subCategories.subCategories.find(element => element.id === itemProject.subCategoryId.match(/\d+/)[0])
                if (subCategory) {
                  listProject[indexProject].categoryId = subCategory.categoryId
                }
              }
            }
          }
        }

      });
    });
    this.setState({
      markers: listProject
    })
    // this.setState({ checkboxChecked: evt.target.checked });
  }


  renderFilter = () => {
    // const data = [{ "name": "test1" }, { "name": "test2" }];
    const listItems = this.state.filter.map((item, index) =>
      <Col xs={4} md={3} lg={12}>
        <div className='d-flex' style={{ flexDirection: 'row', fontSize: 16, marginLeft: 12, whiteSpace: 'nowrap', textOverflow: 'ellipsis', overflow: 'hidden' }}><Form.Check aria-label={item.name} onChange={(e) => this.handleChangeCheckBox(e, item.id)} /><Image src={require('../../../assets/sustainableIcon/' + item.name + '.png')} height={30} width={'auto'} className='sustainableIcon' style={{ marginRight: 8, marginBottom: 8 }} /><div style={{ whiteSpace: 'nowrap', textOverflow: 'ellipsis', overflow: 'hidden' }}>{item.name}</div></div>
      </Col>
    );
    return (
      <Row style={{ width: '100%' }}>
        {listItems}
      </Row>
    );
  }

  render() {
    return (
      <div style={{ width: '100%', fontFamily: 'myriad-pro-condensed', marginRight: 0, marginLeft: 0, borderRadius: 15 }}>
        <Row style={{ marginRight: 0, marginLeft: 0, borderRadius: 15 }}>
          <Col xs={12} md={12} lg={8} className='map' style={{ paddingLeft: 0, paddingRight: 0, borderRadius: 15 }}>
            <MyMapComponent
            style={{borderRadius: 15}}
              isMarkerShown={this.state.isMarkerShown}
              onMarkerClick={this.onMarkerClick}
              goProjectDetail={this.goProjectDetail}
              onClose={this.onMarkerClick}
              onCloseInfo={this.onCloseInfo}
              showId={this.state.showId}
              markers={this.state.markers}
            />
          </Col>
          <Col xs={12} md={12} lg={4} className='d-flex justify-content-between' style={{ flexDirection: 'column', paddingLeft: 0, paddingRight: 0 }}>
            <div className='d-flex justify-content-center align-items-center' style={{ flexDirection: 'column', width: '100%' }}>
              <div style={{ marginBottom: 12 }}>
                <div style={{ fontSize: 32, fontStyle: 'italic' }}>PROJECT BOUNDARY</div>
                <div style={{ height: 5, backgroundColor: '#A51E25', width: '40%' }} />
              </div>
              {/* <div className='d-flex' style={{ flexDirection: 'row', width: '100%' }}>
                <span style={{marginLeft: 42, fontSize: 16}}>Filter</span>
              </div> */}
              {this.renderFilter()}
            </div>
            <div style={{ marginBottom: 12, marginLeft: 12 }}>
            <div style={{marginLeft: 12, fontStyle: 'italic'}}>OUR DEPARTMENT</div>
              <Row style={{ paddingLeft: 0, paddingRight: 0, marginLeft: 0, marginRight: 0 }}>
                <Col xs={6} md={6} lg={6} className='d-flex align-items-center'><div style={{ width: 10, height: 10, backgroundColor: '#A51E26', borderRadius: 5, marginRight: 5 }} /><div>Architecture</div></Col>
                <Col xs={6} md={6} lg={6} className='d-flex align-items-center'><div style={{ width: 10, height: 10, backgroundColor: '#1A2F59', borderRadius: 5, marginRight: 5 }} /><div>Interior Architecture</div></Col>
                <Col xs={6} md={6} lg={6} className='d-flex align-items-center'><div style={{ width: 10, height: 10, backgroundColor: '#F27982', borderRadius: 5, marginRight: 5 }} /><div>Product Design</div></Col>
                <Col xs={6} md={6} lg={6} className='d-flex align-items-center'><div style={{ width: 10, height: 10, backgroundColor: '#5384A4', borderRadius: 5, marginRight: 5 }} /><div>Interior Design</div></Col>
              </Row>
            </div>
          </Col>
        </Row>
      </div>
    )
  }
}

export default enhance(MyFancyComponent);
