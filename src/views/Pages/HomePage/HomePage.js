import React, { Component } from 'react'
import { Container, Row, Col, Alert, Modal, Card, CardDeck } from 'react-bootstrap'
// import Countdown from 'react-countdown'
import { Link } from 'react-router-dom'
// import MetaTags from 'react-meta-tags'
// import Helmet from 'react-helmet'
// import { Slide } from 'react-slideshow-image';
// import renderIf from 'render-if'
// import Swal from 'sweetalert2'
import { connect } from 'react-redux'
import facebook from "../../../assets/img/facebook.png";
import ig from "../../../assets/img/ig.png";
import youtube from "../../../assets/img/youtube.png";
import Fade from 'react-reveal/Fade';

import GGMap from "./ReactGoogleMaps.js";
import ActionUser from '../../../actions/actionUser'
import ActionService from '../../../actions/actionService'
import ActionLottery from '../../../actions/actionLottery'

import homepage from '../../../assets/img/homepage.png'
// import footerImg from '../../../assets/img/footer.png'
import Outbound_Intro from '../../../assets/animation/Outbound_Intro.mp4'

import projects from '../../../data/projects.js'
import images from '../../../data/images.js'
// import profiles from '../../../data/profiles.js'
import subCategories from '../../../data/subCategories.js'
import categories from '../../../data/categories.js'
import YouTube from 'react-youtube';
import Image from 'react-bootstrap/Image'
import ShowMoreText from 'react-show-more-text'
import ImageGallery from 'react-image-gallery'
import { Link as LinkScroll, Element, Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll'

// import {
//   FacebookShareButton,
//   FacebookMessengerShareButton,
//   TwitterShareButton,

// } from "react-share";


// import {
//   FacebookIcon,
//   FacebookMessengerIcon,
//   TwitterIcon,
// } from "react-share";

import './styles.css'

class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      dataYeeGee: [],
      isShowModal: false,
      isSelectValue: false,
      isShowAlert: false,
      textAlert: '',
      valueAll: 0,
      roundId: '',
      onlineUser: 0,
      username: '',
      selectLotto: 1,
      ranLastpage: [],
      categories: categories.categories,
      subCategories: subCategories.subCategories,
      catagoryId: '',
      catagoryName: '',
      listImage: [
        {
          'items': [
            {
              original: `${require('../../../data/project/project1_1.png')}`,
              thumbnail: `${require('../../../data/project/project1_1.png')}`
            },
            {
              original: `${require('../../../data/project/project1_2.png')}`,
              thumbnail: `${require('../../../data/project/project1_2.png')}`
            }
          ]

        },
        {
          'items': [
            {
              original: `${require('../../../data/project/project2_1.png')}`,
              thumbnail: `${require('../../../data/project/project2_1.png')}`
            },
            {
              original: `${require('../../../data/project/project2_2.png')}`,
              thumbnail: `${require('../../../data/project/project2_2.png')}`
            }
          ]

        },
        {
          'items': [
            {
              original: `${require('../../../data/project/project3_1.png')}`,
              thumbnail: `${require('../../../data/project/project3_1.png')}`
            },
            {
              original: `${require('../../../data/project/project3_2.png')}`,
              thumbnail: `${require('../../../data/project/project3_2.png')}`
            }
          ]

        },
        {
          'items': [
            {
              original: `${require('../../../data/project/project4_1.png')}`,
              thumbnail: `${require('../../../data/project/project4_1.png')}`
            },
            {
              original: `${require('../../../data/project/project4_2.png')}`,
              thumbnail: `${require('../../../data/project/project4_2.png')}`
            }
          ]

        },
      ],
      listImage2: [],
      showIndex: false,
      showBullets: false,
      infinite: true,
      showThumbnails: false,
      showFullscreenButton: false,
      showGalleryFullscreenButton: false,
      showPlayButton: false,
      showGalleryPlayButton: false,
      showNav: false,
      isRTL: false,
      slideDuration: 450,
      slideInterval: 5000,
      slideOnThumbnailOver: false,
      thumbnailPosition: 'bottom',
      imageset: true
    }
  }


  async componentDidMount() {
    this.setCategoryImageList()
    // let data = await []
    // let imagesList = await []
    // let profile = null
    // let subCategory = null
    // let category = null
    // await projects.projects.forEach(projectItem => {
    //   imagesList = []
    //   profile = null
    //   subCategory = null
    //   images.images.forEach(imageItem => {
    //     if (projectItem.id === imageItem.projectId) {
    //       imagesList.push(imageItem)
    //     }
    //   });
    //   profile = profiles.profiles.find(profileItem => (projectItem.profileId === profileItem.id));
    //   subCategory = subCategories.subCategories.find(subCategoryItem => (subCategoryItem.id === projectItem.subCategoryId));
    //   category = categories.categories.find(categoryItem => (categoryItem.id === subCategory.categoryId))
    //   data.push({
    //     id: projectItem.id,
    //     projectName: projectItem.projectName,
    //     detailTitle: projectItem.detailTitle,
    //     detailFull: projectItem.detailFull,
    //     latitude: projectItem.latitude,
    //     longitude: projectItem.longitude,
    //     profile: profile,
    //     subCategory: subCategory,
    //     images: imagesList,
    //     CategoryId: category.id,
    //     CategoryName: category.name
    //   })
    // });
    // console.log(await data)
    // this.setState({ data: data })
    // this.randLastProject(data)
  }

  async setCategoryImageList() {
    var catagoryId = [];
    categories.categories.forEach(element => {

      catagoryId.push(element.id);

    });

    var subCategories = {};

    catagoryId.forEach(cate => {
      var subCategorie = []
      this.state.subCategories.forEach(element => {
        if (element.categoryId === cate) {
          subCategorie.push(element.id)
        }
      });
      subCategories[cate] = subCategorie;

    });

    // console.log(typeof (subCategories));
    var aa = [];
    var limit = 4;
    var listimage = [];
    // console.log('projects.projects.length', projects.projects.length)
    // for (var index in subCategories) {
    //   var aaaa = [];
    //   // eslint-disable-next-line no-loop-func
    //   var i = 0;
    //   for (var j = 0; j < projects.projects.length; j++) {

    //     if (subCategories[index].includes(projects.projects[j].subCategoryId ? (projects.projects[j].subCategoryId.match(/\d+/) ? projects.projects[j].subCategoryId.match(/\d+/)[0] : null) : null)) {
    //       let titleImage = images.images.find(image => (image.projectId === projects.projects[j].id && image.isTitle === 'TRUE'));
    //       if (titleImage !== undefined) {
    //         if (i > limit) {
    //           break;
    //         } else {
    //           aaaa.push({
    //             original: `${require('../../../data/project/' + titleImage.image)}`,
    //             thumbnail: `${require('../../../data/project/' + titleImage.image)}`

    //           });
    //           i++;
    //         }

    //       }

    //     }
    //   }

    //   aa.push(aaaa);

    // }
    // console.log('subCategories', subCategories)
    var projectImCategory = [];
    // for (var index in subCategories) {
    //   if (subCategories[index].includes(projects.projects[j].subCategoryId ? (projects.projects[j].subCategoryId.match(/\d+/) ? projects.projects[j].subCategoryId.match(/\d+/)[0] : null) : null)) {
    //     projectImCategory
    //   }
    // }

    for (var j = 0; j < projects.projects.length; j++) {
      for (var index in subCategories) {
        if (subCategories[index].includes(projects.projects[j].subCategoryId ? (projects.projects[j].subCategoryId.match(/\d+/) ? projects.projects[j].subCategoryId.match(/\d+/)[0] : null) : null)) {
          if (projectImCategory[index] == undefined) {
            projectImCategory[index] = [];
          }
          projectImCategory[index].push(projects.projects[j].id);
        }
      }
    }
    // console.log('projectImCategory', projectImCategory);

    var randomProjectInCategory = [];
    let listimage3 = []
    for (var ii = 1; ii < projectImCategory.length; ii++) {
      let listsize = projectImCategory[ii].length;
      let randomArr = [];
      let randomArr2 = [];
      let aaaa = [];
      let i = 0;
      let maxsize = 5;
      // console.log('Listsize', listsize)
      if (listsize > 0) {


        while (i < maxsize) {
          // var r =Math.random() * max
          let r = Math.floor(Math.random() * listsize);
          // console.log("Home -> setCategoryImageList -> r", r)
          // console.log("Home -> setCategoryImageList -> projects.projects[projectImCategory[ii][r]].id", projects.projects[projectImCategory[ii][r]])

          if (!randomArr.includes(projectImCategory[ii][r])) {


            let titleImage = images.images.find(image => (image.projectId === projectImCategory[ii][r] && image.isTitle === 'TRUE'));
            // console.log("Home -> setCategoryImageList -> titleImage", titleImage)
            if (titleImage !== undefined) {
              aaaa.push({
                original: `${require('../../../data/project/' + titleImage.image)}`,
                thumbnail: `${require('../../../data/project/' + titleImage.image)}`
              });

              randomArr.push(projectImCategory[ii][r]);
              i++;
            }
            // else {
            //     aaaa.push({
            //       original: `${require('../../../data/project/temp.png')}`,
            //       thumbnail: `${require('../../../data/project/temp.png')}`
            //     });
            //   }





            // randomArr.push(projectImCategory[ii][r]);
            // i++;

          }
          if (!randomArr2.includes(projectImCategory[ii][r])) {
            randomArr2.push(projectImCategory[ii][r]);

          }

          if (randomArr2.length === listsize) {
            i = maxsize + 1;
          }

        }
      }
      listimage3.push(aaaa)
      randomProjectInCategory.push(randomArr);
    }
    // console.log("Home -> setCategoryImageList -> templist", listimage3)

    // console.log('randomProjectInCategory', randomProjectInCategory)
    // var listimage3 = [];
    // for (var cate of randomProjectInCategory) {
    //   var aaaa = [];
    //   cate.forEach(cate => {
    //     console.log("Home -> setCategoryImageList -> cate", cate)

    //     let titleImage = images.images.find(image => (image.projectId === cate && image.isTitle === 'TRUE'));
    //     console.log("Home -> setCategoryImageList -> titleImage", titleImage)
    //     if (titleImage !== undefined) {
    //       aaaa.push({
    //         original: `${require('../../../data/project/' + titleImage.image)}`,
    //         thumbnail: `${require('../../../data/project/' + titleImage.image)}`
    //       });
    //     } else {
    //       aaaa.push({
    //         original: `${require('../../../data/project/temp.png')}`,
    //         thumbnail: `${require('../../../data/project/temp.png')}`
    //       });
    //     }

    //     // console.log(cate)
    //   })

    //   listimage3.push(aaaa);

    // }
    // console.log('listimage3', listimage3);

    await this.setState({
      listImage2: listimage3,
      imageset: false
    });

    // console.log('aa', aa);
    // console.log('listImage2', this.state.listImage2);

    // console.log('catagoryId' , catagoryId);

  }


  randLastProject = (data) => {
    let ranArrNum = [];
    while (ranArrNum.length < 4) {
      let ranNumber = Math.floor(Math.random() * data.length) + 1;
      if (!ranArrNum.includes(ranNumber)) {
        ranArrNum.push(ranNumber)
      }
    }
    this.setState({ ranLastpage: ranArrNum })
  }

  // render4Lastpage = () => {
  //   const listItem = this.state.ranLastpage.map((data, index) =>
  //     // <Card style={{ borderRadius: 10, fontFamily: 'myriad-pro-condensed' }}>
  //     //   <Link
  //     //     to={{
  //     //       pathname: "/details/" + this.state.data[data - 1].id
  //     //     }}
  //     //   >
  //     //     <Card.Img fluid variant="top" src={require('../../../data/project/' + this.state.data[data - 1].images[0].image)} style={{ height: '150px', objectFit: 'cover' }} />
  //     //     <Card.Body>
  //     //       <Card.Title style={{ textDecoration: 'none!important', color: '#192F58', fontSize: 16 }}>{this.state.data[data - 1].projectName}</Card.Title>
  //     //     </Card.Body>
  //     //   </Link>
  //     // </Card>
  //     <Col xs={12} md={4} style={{ flex: 1, maxWidth: '250px!important', backgroundColor: 'white', padding: 0, margin: 5, borderRadius: 10 }}>
  //       <Link
  //         to={{
  //           pathname: "/details/" + this.state.data[data - 1].id
  //         }}
  //       >
  //         <div className="wrapper">
  //           <Image src={require('../../../data/project/' + this.state.data[data - 1].images[0].image)} />
  //         </div>
  //         <div className='d-flex justify-content-center' style={{ width: '100%' }}>
  //           <span style={{ fontSize: 32, textDecoration: 'none!important', color: '#192F58', width: '100%', textAlign: 'center' }}>{this.state.data[data - 1].projectName}</span>
  //         </div>
  //       </Link>
  //     </Col>
  //   );

  //   return (
  //     <>
  //       {listItem}
  //     </>
  //   )
  // }

  selectCategory = (id) => {

    let category = this.state.categories.find(element => (element.id === id));

    this.setState({
      catagoryId: id,
      catagoryName: category === undefined ? '' : category.name
    })
    scroller.scrollTo('subCategory', {
      duration: 800,
      delay: 0,
      smooth: 'easeInOutQuart'
    })
  }

  categories = () => {
    if (this.state.imageset) {
      return null
    } else {


      // console.log("categories -> categories", this.state.categories)
      const listItem = this.state.categories.map((data, index) =>

        // <Card  style={{ borderRadius: 10, boxShadow: (this.state.catagoryId === data.id ? '5px 10px #E9DDC9' : '') }}
        // onClick={() => this.selectCategory(data.id)} key={index}
        // >
        <Col xs={6} md={3} lg={3} className='wrapper' style={{ margin: '30px 0px' }}>
          <Link
            to={{
              pathname: '/category/' + (index + 1),
            }}>
            <ImageGallery
              ref={i => this.listImage = i}
              items={this.state.listImage2[index]}
              lazyLoad={false}
              onImageLoad={this._onImageLoad}
              onSlide={this._onSlide.bind(this)}
              onPause={this._onPause.bind(this)}
              onScreenChange={this._onScreenChange.bind(this)}
              onPlay={this._onPlay.bind(this)}
              infinite={this.state.infinite}
              showBullets={this.state.showBullets}
              showFullscreenButton={this.state.showFullscreenButton && this.state.showGalleryFullscreenButton}
              showPlayButton={this.state.showPlayButton && this.state.showGalleryPlayButton}
              showThumbnails={this.state.showThumbnails}
              showIndex={this.state.showIndex}
              showNav={this.state.showNav}
              isRTL={this.state.isRTL}
              thumbnailPosition={this.state.thumbnailPosition}
              slideDuration={parseInt(this.state.slideDuration)}
              slideInterval={parseInt(this.state.slideInterval)}
              slideOnThumbnailOver={this.state.slideOnThumbnailOver}
              additionalClass="app-image-gallery"
              autoPlay={true}
            />
            {/* <Card.Img fluid="true" variant="top" src={require('../../../data/project/project1_4.png')} className="wrapper" /> */}
            <div style={{ margin: '30px 0px', textAlign: 'center', cursor: 'pointer', fontFamily: 'myriad-pro-condensed', color: '#192f58', textDecoration: 'none' }}>{data.name}</div>
          </Link>
          {/* </Card> */}
        </Col>
      );

      return (
        <>
          {listItem}
        </>
      )
    }
  }

  isSafari = () => {
    const ua = navigator.userAgent.toLowerCase();
    return ua.indexOf("safari") > -1 && ua.indexOf("chrome") < 0;
  };

  render() {
    return (
      <div style={{ background: '#F8F5EF', margin: '0px 0px 0px 0px' }}>
        <div style={{ paddingTop: 10 }}>
          <Container style={{ marginTop: 40 }}>
            <Row xs={12} md={12} lg={12} className='d-flex justify-content-xs-center align-items-xs-center' style={{ backgroundColor: 'white', borderRadius: 15 }}>
              <Col xs={{ span: 12, order: 2 }} md={{ span: 3, order: 1 }} lg={{ span: 3, order: 1 }} className='d-flex justify-content-center' style={{ flexDirection: 'column' }}>
                <div className='d-flex justify-content-between align-items-center' style={{ flexDirection: 'column', height: '100%', marginBottom: 12 }}>
                  <div className='d-flex justify-content-between' style={{ flexDirection: 'column', height: '100%' }}>
                    <div style={{ fontSize: 32, color: '#192F58', fontFamily: 'myriad-pro-condensed', fontStyle: 'italic', matginLeft: 0, marginRight: 0 }}>
                      OUR CONCEPT
            </div>
                    <div className='ourconcept-web' style={{ fontFamily: 'myriad-pro-condensed', fontSize: 16 }}>
                      Let’s walk out <br />
                    across the boundary of <br />
                    yours , <br />
                    step ahead and  <br />
                    experience  <br />
                    the design work  <br />
                    that will take you  <br />
                    to the new aspects
            </div>
            <div className='ourconcept-mobile' style={{ fontFamily: 'myriad-pro-condensed', fontSize: 16 }}>
                      Let’s walk out
                    across the boundary of 
                    yours , <br />
                    step ahead and  
                    experience  
                    the design work  <br />
                    that will take you  
                    to the new aspects
            </div>
                    <div style={{ fontFamily: 'myriad-pro-condensed', color: '#192F58' }}>
                      #OUTBOUND2020
               </div>
                  </div>
                </div>
              </Col>
              <Col xs={{ span: 12, order: 1 }} md={{ span: 9, order: 2 }} lg={{ span: 9, order: 2 }} style={{ padding: 0 }}>
                {/* <Image src={Outbound_Intro} fluid /> */}
                {this.isSafari() ? <Image src={homepage} alt="Muted Video" fluid /> : <video style={{ borderRadius: 15 }} width="100%" height="100%" id="vid" autoPlay autoplay="autoplay" muted={true}>
                  <source src={Outbound_Intro} type="video/mp4" />
                </video>}
                {/* <video width="100%" height="100%" id="vid" autoPlay autoplay="autoplay" muted={true}>
                <source src={Outbound_Intro} type="video/mp4" />
              </video> */}
                {/* <iframe src={Outbound_Intro} allow="autoplay"></iframe> */}
                {/* <video  src={require('../../../data/123a.webm')} autoPlay /> */}
              </Col>
            </Row>

            <Row className='d-flex justify-content-center' style={{ margin: '30px 0px' }}>
              {/* lastest project */}
              <Col xs={12} md={12} style={{ padding: 0 }}>
                <Row md={12}  >
                  <Col md={6}>
                    <div className='d-flex justify-content-center align-items-center ' style={{ flexDirection: 'column' }}>
                      <span className='lastestcss' style={{ fontSize: 32, textAlign: 'center', width: '100%', fontFamily: 'myriad-pro-condensed', fontStyle: 'italic' }}>
                        LASTEST PROJECT
                    </span>

                      <div style={{ height: 5, backgroundColor: '#A51E25', width: '50%', marginBottom: 20 }} />

                    </div>
                  </Col>
                </Row>
                {/* //render 4 image */}
                {/* <CardDeck style={{ margin: '30px 0px' }}> */}
                {/* {this.render4Lastpage()}    */}
                {/* <CardDeck> */}

                <Row style={{ margin: '30px 0px' }}>
                  {this.categories()}
                  {/* </CardDeck> */}
                  {/* </CardDeck> */}
                  {/* end//render 4 image */}
                </Row>
              </Col>
              {/* end lastest project */}
            </Row>

            <Row style={{ backgroundColor: 'white', margin: '30px 0px', borderRadius: 15 }}>
              <GGMap />
            </Row>


            <Row className='d-flex justify-content-center' style={{ margin: '30px 0px' }}>
              <div>
                <span style={{ fontSize: 32, textAlign: 'center', width: '100%', fontFamily: 'myriad-pro-condensed', fontStyle: 'italic' }}>
                  ABOUT US
                    </span>
                <div className='d-flex flex-row-reverse'>
                  <div style={{ height: 5, backgroundColor: '#1A2F59', width: '50%', marginBottom: 12 }} />
                </div>
              </div>
            </Row>
            <Row style={{ margin: '30px 0px' }}>
              {/* youtube video */}
              <Col s={12} md={8}>
                <div className='mainYT' >
                  <YouTube
                    videoId="6L1majfaz_8"
                    opts={{
                      height: '100%',
                      width: '100%',
                      padding: '20'
                    }}
                  />
                </div>
              </Col>
              {/* end youtube video */}

              {/* promote video */}
              <Col xs={12} md={4} style={{ textAlign: 'center', fontFamily: 'myriad-pro-condensed' }}>

                <Row style={{ fontSize: 16, textAlign: 'left' }}>

                  <ShowMoreText
                    /* Default options */
                    lines={10}
                    more='Read more'
                    less='Read less'
                    anchorClass=''
                    onClick={this.executeOnClick}
                  // expanded={false}
                  // width={330}
                  >
                    Out of your boundary : The implication of this particular word defined into two term. The first boundary, the graduation from academic and start our new journey as a designer. The second boundary was due to an unexceptional condition of the covid-19, we come across a New limitation of new normal lifestyle opening new perceptions.
                  <br /><br />
                  Through the past year, we have learned that the scope of the architectural works does not limit to the design part. However, the architect is also responsible for various types of works such as research, analysis, construction planning, etc. All this aspects brought us out from our comfort zone which is the main reason for this thesis exhibition “Out of your boundary”
                  <br /><br />
                  Let’s explore the Architecture graduated student from Assumption University year 2019 that put heart and souls into their work. From the Architecture 5 years, Interior Architecture 5 years, Interior Design 4 years, and lastly Product Design 4 years.
            </ShowMoreText>
                </Row>

              </Col>
              {/* end promote video */}
            </Row>
            {/* <Row className='d-flex justify-content-center yt4' style={{ margin: '30px 0px' }}> */}
            <Row className='yt4'>
              <Col sm={12} md={6} lg={3} >
                <Fade>
                  <YouTube videoId="-wSO_TRC0vo" onReady={this._onReady} className="youtube" />
                  {/* <h3 className="youtube-title">Special Guest</h3>
                  <h6 className="youtube-title">สถาปัตย์ลาดกระบัง 20 ปีที่แล้ว</h6> */}
                </Fade>
              </Col>
              <Col sm={12} md={6} lg={3} >
                <Fade>
                  <YouTube videoId="FTPvzNuC140" onReady={this._onReady} className="youtube" />
                  {/* <h3 className="youtube-title">Special Guest</h3>
                  <h6 className="youtube-title">สถาปัตย์ลาดกระบัง 20 ปีที่แล้ว</h6> */}
                </Fade>
              </Col>
              <Col sm={12} md={6} lg={3} >
                <Fade>
                  <YouTube videoId="giRcZQ-Iotg" onReady={this._onReady} className="youtube" />
                  {/* <h3 className="youtube-title">Special Guest</h3>
                  <h6 className="youtube-title">สถาปัตย์ลาดกระบัง 20 ปีที่แล้ว</h6> */}
                </Fade>
              </Col>
              <Col sm={12} md={6} lg={3} >
                <Fade>
                  <YouTube videoId="2gMgJ9IYUcM" onReady={this._onReady} className="youtube" />
                  {/* <h3 className="youtube-title">Special Guest</h3>
                  <h6 className="youtube-title">สถาปัตย์ลาดกระบัง 20 ปีที่แล้ว</h6> */}
                </Fade>
              </Col>
            </Row>


          </Container >
          {/* footer section */}

          {/* <Row className='d-flex justify-content-center' style={{ textAlign: 'center', margin: '40px 20px', padding: '0px 100px' }} >
            <Container>
              <Col xs={12} md={4} >
                <div className='d-flex justify-content-center' style={{ fontSize: 32, width: '100%', color: '#A51E25' }}>
                  <span style={{ fontFamily: 'myriad-pro-condensed', fontStyle: 'italic' }}>
                    HI THERE! <br/>
                    LETS GATHER HERE TO JOIN OUR ACTIVITY!!</span>
                </div>
              </Col>
              <Col xs={12} md={6}  >
              </Col>
            </Container>
          </Row> */}
          <Row style={{ backgroundColor: 'rgba(165, 30, 37)', padding: '20px 0px 20px 0px', fontFamily: 'myriad-pro-condensed', marginTop: 32 }}>
            <Container>
              <Row>
                <Col xs={12} md={12} >
                  {/* <Row xs={12} md={12} className='d-flex justify-content-center' style={{ color: '#E8DDC9', marginTop: 12, marginBottom: 12, fontSize: 16, textAlign: 'center', fontFamily: 'myriad-pro-condensed' }} >
                    “ Out of Boundary ” giveaway activity!<br />
                    Just follow Facebook and Instagram  Outbound : AAU Thesis Exhibition 2020 <br />
                    and shared your favorite project on your Facebook timeline hashtag<br />
                  
                  #outbound2020 #ข้ามเส้น2020 #aauthesisexhibition
                  
                  <br />
                    <br />
                  Lucky winners will receive Outbound Tote Bag and Mask!
                  <br />
                    <br />
                  The Winner will announced via Facebook Outbound :     <br />
                  AAU Thesis Exhibition 2020...stay tuned!
                </Row> */}

                  <Row xs={12} md={12} className='justify-content-center align-items-center' style={{ height: '100%' }} >
                    {/* <Col className='align-self-end'> */}
                    <a className='align-self-center' href="https://www.facebook.com/Thesisexhibition2020" style={{ color: '#E8DDC9', marginRight: 32 }} target="_blank">
                      <img
                        alt="https://www.facebook.com/Thesisexhibition2020"
                        src={facebook}
                        // width="30"
                        height="40"
                        className="d-inline-block align-bottom"
                      /></a>
                    {/* </Col>
                  <Col className='align-self-end'> */}
                    <a className='align-self-center' href="https://www.instagram.com/outbound_2020/" style={{ color: '#E8DDC9', marginRight: 32 }} target="_blank">
                      <img
                        alt=""
                        src={ig}
                        // width="30"
                        height="40"
                        className="d-inline-block "
                      /></a>
                    {/* </Col>
                  <Col className='align-self-end'> */}
                    <a className='align-self-center' href="https://www.youtube.com/channel/UCOf32iPFo2J2qZnW-ihL6YQ" style={{ color: '#E8DDC9' }} target="_blank">
                      <img
                        alt=""
                        src={youtube}
                        // width="30"
                        height="40"
                        className="d-inline-block   "
                      /></a>
                    {/* </div> */}
                    {/* </Col> */}
                  </Row>
                </Col>
                {/* <Col xs={12} md={8} className='d-flex justify-content-center ' style={{ height: '100%', alignItems: 'center' }}>
                  <Image className='imagefoot' fluid src={footerImg} style={{ alignItems: 'center' }} />
                </Col> */}
              </Row>
            </Container>
          </Row>
        </div></div>
    )
  }

  _onImageClick(event) {
    console.debug('clicked on image', event.target, 'at index', this._imageGallery.getCurrentIndex());
  }

  _onImageLoad(event) {
    console.debug('loaded image', event.target.src);
  }

  _onSlide(index) {
    // this._resetVideo();
    console.debug('slid to index', index);
  }

  _onPause(index) {
    console.debug('paused on index', index);
  }

  _onScreenChange(fullScreenElement) {
    console.debug('isFullScreen?', !!fullScreenElement);
  }

  _onPlay(index) {
    console.debug('playing from index', index);
  }

  _handleInputChange(state, event) {
    this.setState({ [state]: event.target.value });
  }

  _handleCheckboxChange(state, event) {
    this.setState({ [state]: event.target.checked });
  }

  _handleThumbnailPositionChange(event) {
    this.setState({ thumbnailPosition: event.target.value });
  }



}

const mapStateToProps = (state) => ({
  user: state.user
})

const mapDispatchToProps = (dispatch) => ({
  storeUserAccessToken: (accessToken) => {
    dispatch({
      type: ActionUser.STORE_USER_ACCESS_TOKEN,
      accessToken: accessToken
    })
  },
  storeUserBalance: (balance) => {
    dispatch({
      type: ActionUser.STORE_BALANCE,
      balance: balance
    })
  },
  storeServiceInfo: (
    id,
    image,
    name,
    roundId,
    service_endtime,
    service_starttime
  ) => {
    dispatch({
      type: ActionService.STORE_SERVICE_INFO,
      id: id,
      image: image,
      name: name,
      roundId: roundId,
      service_endtime: service_endtime,
      service_starttime: service_starttime
    })
  },
  setHardMode: () => {
    dispatch({
      type: ActionLottery.SET_HARD_MODE
    })
  },
  resetDataEzMode: () => {
    dispatch({
      type: ActionLottery.RESET_DATA_EZ_MODE
    })
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(Home)
