import React, { Component } from "react";
import { Container, Row, Col, Alert, Image, Button } from 'react-bootstrap';
import { Link as LinkScroll, Element, Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll'
import Countdown from 'react-countdown';
import { Link } from "react-router-dom";
import { Modal } from 'react-bootstrap';
// import { Slide } from 'react-slideshow-image';
import renderIf from 'render-if';
import Swal from "sweetalert2";
import { connect } from "react-redux";
import ImageGallery from 'react-image-gallery';
import ActionUser from "../../../actions/actionUser";
import ActionService from "../../../actions/actionService";
import ActionLottery from "../../../actions/actionLottery";
import projects from '../../../data/projects.js'
import profiles from '../../../data/profiles.js'
import images from '../../../data/images.js'
import './styles.css';
import YouTube from 'react-youtube';
import {
  FacebookShareButton,
  FacebookMessengerShareButton,
  TwitterShareButton,
  FacebookIcon,
  FacebookMessengerIcon,
  TwitterIcon,

} from "react-share";

const PREFIX_URL = 'https://raw.githubusercontent.com/xiaolin/react-image-gallery/master/static/';
class DetailsPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      validId: false,
      project: {},
      profile: {},
      images: [],
      randomProject: [],
      randomProject2: [],
      subCategoryP: [],
      imagesRender: [],
      showIndex: false,
      showBullets: true,
      infinite: true,
      showThumbnails: true,
      showFullscreenButton: true,
      showGalleryFullscreenButton: true,
      showPlayButton: false,
      showGalleryPlayButton: true,
      showNav: true,
      isRTL: false,
      slideDuration: 10,
      slideInterval: 10,
      slideOnThumbnailOver: false,
      show: false,
      thumbnailPosition: 'bottom',

    };

    // this.images = [
    // ].concat(this._getStaticImages());

  }

  project(id) {
    // console.log(id)
    var obj;
    for (let index = 0; index < projects.projects.length; index++) {
      // console.log('projects' , projects.projects[index])

      if (projects.projects[index].id.toString() === id.toString()) {

        obj = projects.projects[index];
        break;
      }
    }

    return obj;
    // return '5';
  }

  setRandomProject(subCategoryId, ids) {
    // console.log(setRandomProject )
    let subCategoryP = [];
    // console.log('ids', ids)
    for (let index = 0; index < projects.projects.length; index++) {
      // console.log(projects.projects[index].id)

      if (subCategoryId === projects.projects[index].subCategoryId && projects.projects[index].id !== ids) {
        subCategoryP.push(projects.projects[index]);
      }
    }

    let maxsize = 3;
    let listsize = subCategoryP.length;
    // console.log('subCategoryP', subCategoryP)
    // let max = projects.projects.length;
    // console.log('max' , max)
    let randomArr = [];
    let randomArr2 = [];
    let randomArrB = [];
    let randomArr2B = [];
    let i = 0;
    // console.log('Listsize', listsize)
    if (listsize > 0) {


      while (i < maxsize) {
        // var r =Math.random() * max
        let r = Math.floor(Math.random() * listsize);
        // console.log('setRandomProject', r);
        if (!randomArr.includes(r)) {
          randomArr.push(r);
          i++;
        }
        if (!randomArr2.includes(r)) {
          randomArr2.push(r);

        }

        if (randomArr2.length === listsize) {
          i = maxsize;
        }

      }



      i = 0;
      while (i < maxsize) {
        // var r =Math.random() * max
        let r = Math.floor(Math.random() * listsize);
        // console.log('setRandomProject', r);
        if (!randomArrB.includes(r)) {
          randomArrB.push(r);
          i++;
        }
        if (!randomArr2B.includes(r)) {
          randomArr2B.push(r);

        }

        if (randomArr2B.length === listsize) {
          i = maxsize;
        }

      }
    }

    // var ss = Math.random() * max

    // console.log('randomArlength', randomArr.length);

    if (randomArr.length < 1) {
      let maxsize = 3;
      let listsize = projects.projects.length;
      // console.log('listsize', listsize);
      if (listsize > 0) {

        i = 0;
        while (i < maxsize) {
          // var r =Math.random() * max
          let r = Math.floor(Math.random() * listsize);
          // console.log('setRandomProject', r);
          if (!randomArr.includes(r)) {
            randomArr.push(r);
            i++;
          }
          if (!randomArr2.includes(r)) {
            randomArr2.push(r);

          }

          if (randomArr2.length === listsize) {
            i = maxsize;
          }

        }
      }


      subCategoryP = projects.projects

      // console.log('arr', randomArr)
    }



    if (randomArrB.length < 1) {
      let maxsize = 3;
      let listsize = projects.projects.length;

      if (listsize > 0) {

        i = 0;
        while (i < maxsize) {
          // var r =Math.random() * max
          let r = Math.floor(Math.random() * listsize);
          // console.log('setRandomProject', r);
          if (!randomArrB.includes(r)) {
            randomArrB.push(r);
            i++;
          }
          if (!randomArr2B.includes(r)) {
            randomArr2B.push(r);

          }

          if (randomArr2B.length === listsize) {
            i = maxsize;
          }

        }
      }


      // subCategoryP = projects.projects

      // console.log('arrB', randomArrB)
    }



    this.setState({
      randomProject: randomArr,
      randomProject2: randomArrB,
      subCategoryP: subCategoryP
    });
    // console.log('randomArr', randomArr);
  }

  images(id) {

    // console.log(id)
    // var obj;
    var imagesList = [];
    for (let index = 0; index < images.images.length; index++) {
      // console.log('projects' , projects.projects[index])

      if (images.images[index].projectId.toString() === id.toString()) {

        imagesList.push(images.images[index]);
      }
    }
    // console.log('dsadasdasd', imagesList)
    this.setImage(imagesList);
    // return imagesList;

  }

  profile(id) {

    var obj;
    for (let index = 0; index < profiles.profiles.length; index++) {
      // console.log('projects' , projects.projects[index])

      if (profiles.profiles[index].id.toString() === id.toString()) {

        obj = profiles.profiles[index];
        break;
      }
    }
    return obj;

  }
  init(id) {
    var project = this.project(id);
    // console.log('project', project)
    if (project === undefined) {
      this.setState({
        validId: false
      });
    } else {
      this.setState({
        project: project
      });
      this.setRandomProject(project.subCategoryId, project.id);
      var profile = this.profile(project.profileId);
      // console.log('profile', profile)
      if (profile !== undefined) {
        this.setState({
          profile: profile,
          validId: true
        });

        this.images(id);
      } else {
        this.setState({
          // profile: profile,
          validId: false
        });
      }



    }
  }
  async componentDidMount() {
    let id = this.props.match.params.id
    this.init(id)
    // console.log('Math.random()' , Math.random())





  }

  setImage(images) {
    let imagesForSet = [];

    for (let i = 0; i < images.length; i++) {
      // if (i !== 0) { //don't want first image
      try {
        imagesForSet.push({
          original: `${require('../../../data/project/' + images[i].image)}`,
          thumbnail: `${require('../../../data/project/' + images[i].image)}`,

        });
      } catch (error) {
        console.log(error)
      }

      // }

    }



    // console.log('imagesForSet', imagesForSet);

    this.setState({
      imagesRender: imagesForSet,
      images: images
    });

  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.slideInterval !== prevState.slideInterval ||
      this.state.slideDuration !== prevState.slideDuration) {
      // refresh setInterval
      this._imageGallery.pause();
      this._imageGallery.play();
    }
  }
  redirect = (id) => {
    // let id = this.props.match.params.id
    scroll.scrollToTop();
    // console.log(id)
    // window.location.reload();
    this.init(id)
  }

  renderImage() {
    // console.log('this.state.images', this.state.images);
    var render = [];



    render.push(<Row>
      <Col lg="6">
        {
          this.state.images[0] !== undefined ?
            <div >
              <img alt="test" src={require('../../../data/project/' + this.state.images[1].image)} className="image-show" />
            </div> : ''
        }
      </Col>
      <Col lg="6">
        {
          this.state.images[1] !== undefined ?
            <div >
              <img alt="test" src={require('../../../data/project/' + this.state.images[2].image)} className="image-show" />
            </div> : ''
        }
      </Col>
      <Col>
        <br/>
        <br/>
        <div>
          {this.state.project.detail4}
        </div>
        <br/> <br/>
      </Col>
      <Col lg="12" style={{ marginBottom: '10px' }}>
        {
          this.state.images[2] !== undefined ?
            <div >
              <img alt="test" src={require('../../../data/project/' + this.state.images[3].image)} className="image-show" />
            </div> : ''
        }
      </Col>
      <Col lg="6">
        {
          this.state.images[3] !== undefined ?
            <div >
              <img alt="test" src={require('../../../data/project/' + this.state.images[4].image)} className="image-show" />
            </div> : ""
        }
      </Col>


      <Col lg="6">
        {
          this.state.images[4] !== undefined ?
            <div >
              <img alt="test" src={require('../../../data/project/' + this.state.images[5].image)} className="image-show" />
            </div> : ''
        }
      </Col>




    </Row>)

    return render;
  }

  renderRandomProject() {
    // console.log('ggg', images)

    var render = [];
    if (this.state.randomProject.length > 0) {
      render.push(<div className="side-header theme-red">
        <h5> Project that you may like</h5>
      </div>)
    }

    for (let i = 0; i < this.state.randomProject.length; i++) {
      let titleImage = images.images.find(element => (element.projectId === this.state.subCategoryP[this.state.randomProject[i]].id && element.isTitle === 'TRUE'));
      if (titleImage) {
        render.push(<div className="none-a">
          <Link
            to={{
              pathname: "/Details/" + this.state.subCategoryP[this.state.randomProject[i]].id
            }}
            // params={{ id: this.state.subCategoryP[this.state.randomProject[i]].id}}
            onClick={() => {
              this.redirect(this.state.subCategoryP[this.state.randomProject[i]].id)
            }
            }
          >
            <Row key={i} className="project-that-you-may-like no-margin" >
              <Col lg="6" md="6" style={{ overflow: 'hidden' }} className="nopadding">
                <img alt="test" src={require('../../../data/project/' + titleImage.image)} className="image-side-that-you-may-like may-like" />
              </Col>
              <Col lg="6" md="6" >
                <div className="projectName">
                  <h4>{this.state.subCategoryP[this.state.randomProject[i]].projectName}</h4>
                </div>
                <div className="text-line-limit">
                  {this.state.subCategoryP[this.state.randomProject[i]].location}
                </div>
              </Col>

            </Row>
          </Link>
        </div>)
      }

    }



    return render;
  }


  getYoutubeId = (ytUrl) => {

    var pattern1 = "watch?v=";
    var pattern2 = "&list=";

    var str2 = ytUrl.substr(ytUrl.indexOf(pattern1) + pattern1.length, (ytUrl.indexOf(pattern2) - pattern1.length - ytUrl.indexOf(pattern1)));
    // console.log('str2', str2);
    // console.log('ytUrl.indexOf(pattern1)', ytUrl.indexOf(pattern1));
    // console.log('ytUrl.indexOf(pattern2)', ytUrl.indexOf(pattern2));
    return str2;


  }
  renderRecommended = () => {
    var render = [];
    if (this.state.randomProject2.length > 0) {
      render.push(<Col lg="12">
        <div className="side-header theme-red">
          <h5> Recommended For You</h5>
        </div>
      </Col>)
    }
    var card = [];
    for (let i = 0; i < this.state.randomProject2.length; i++) {
      let titleImage = images.images.find(element => (element.projectId === this.state.subCategoryP[this.state.randomProject2[i]].id && element.isTitle === 'TRUE'));
      if (titleImage) {
        card.push(
          <Col lg="4" className="none-a" >
            <Link
              to={{
                pathname: "/Details/" + this.state.subCategoryP[this.state.randomProject2[i]].id
              }}

              onClick={() => this.redirect(this.state.subCategoryP[this.state.randomProject2[i]].id)}
            >
              <Row key={i} className="project-that-you-may-like">
                <Col lg="6" md="4" xs="4" style={{ overflow: 'hidden' }} className="nopadding">
                  <img alt="test" src={require('../../../data/project/' + titleImage.image)} className="image-side-that-you-may-like may-like" />
                </Col>
                <Col lg="6" md="8" xs="8" >
                  <div>
                    <h4 className="text-line-limit italic">{this.state.subCategoryP[this.state.randomProject2[i]].projectName}</h4>
                  </div>
                  <div>
                    <p className="text-line-limit">{this.state.subCategoryP[this.state.randomProject2[i]].location}</p>
                  </div>
                </Col>
              </Row>
            </Link>
          </Col>

        )
      }
    }
    render.push(<Row>
      {card}
    </Row>)


    return render;
  }

  _getStaticImages(id) {
    let images = [];
    for (let i = 2; i < 12; i++) {
      images.push({
        original: require,
        thumbnail: `${PREFIX_URL}${i}t.jpg`,
        description: 'Test Description'
      });
    }

    return images;
  }

  _onImageClick(event) {
    console.debug('clicked on image', event.target, 'at index', this._imageGallery.getCurrentIndex());
  }

  _onImageLoad(event) {
    console.debug('loaded image', event.target.src);
  }

  _onSlide(index) {
    // this._resetVideo();
    console.debug('slid to index', index);
  }

  _onPause(index) {
    console.debug('paused on index', index);
  }

  _onScreenChange(fullScreenElement) {
    console.debug('isFullScreen?', !!fullScreenElement);
  }

  _onPlay(index) {
    console.debug('playing from index', index);
  }

  _handleInputChange(state, event) {
    this.setState({ [state]: event.target.value });
  }

  _handleCheckboxChange(state, event) {
    this.setState({ [state]: event.target.checked });
  }

  _handleThumbnailPositionChange(event) {
    this.setState({ thumbnailPosition: event.target.value });
  }

  handleClose = () => {
    this.setState({
      show: false
    })
  }

  handleShow = () => {
    this.setState({
      show: true
    })
  }
  onClickSaveImage = (postcard) => {
    // console.log('postcard', postcard);
    window.open(require('../../../data/postcard/' + this.state.project.postcardImage), "_blank")
  }
  toPort = (newUrl) => {
    // console.log('ss')
    window.open(newUrl, "_blank")
  }


  render() {
    // console.log('innerHeight',innerWidth)
    if (this.state.validId) {
      return (
        <div style={{ background: '#F8F5EF', margin: '0px' }}>
        <div style={{ paddingTop: 0 }}>

        <Container style={{  background: '#F8F5EF', paddingBottom: '-50px', fontFamily: 'myriad-pro-condensed' }}>
         
          <Row className="no-margin">

            <Col lg="3" md="12"  className="side-col-profile profile">

              <div>
                <Row>
                  <Col lg="12" md="12" className='d-flex justify-content-center'>
                    {this.state.profile.imageProfile !== '' && this.state.profile.imageProfile ?
                      <Image alt="test" src={require('../../../data/profile/' + this.state.profile.imageProfile)} className="image-side" roundedCircle />
                      : null
                    }
                  </Col>
                  <Col lg="12">

                    <div className="side-profile-header">
                      <h5> {this.state.profile.name}</h5>
                    </div>
                    <div className="red-line">  
                    <div className="sub-bottom">

                     {this.state.profile.portfolioUrl?<h6><Link onClick={() => this.toPort(this.state.profile.portfolioUrl)}>Visit Portfolio &#8594; </Link></h6>:''} 
                     {/* <h6><Link onClick={() => this.toPort(this.state.profile.portfolioUrl)}>Visit Portfolio &#8594; </Link></h6> */}
                    </div>
                    <br />
                    <div className="side-profile">
                      <h6 className="profile-sub-title"> Adviser </h6>
                      <h6 className="text-b"> {this.state.profile.adviser}</h6>
                    </div>
                    <div className="side-profile mb-4">
                      <h6 className="profile-sub-title"> Location </h6>
                      <h6 className="text-b"> {this.state.project.location}</h6>
                    </div>
                    
                    <div className="side-profile">
                      <h6 className="profile-sub-title"> Facebook </h6>
                      <h6 className="text-b"> {this.state.profile.facebook}</h6>
                    </div>
                    <div className="side-profile">
                      <h6 className="profile-sub-title"> Instagram </h6>
                      <h6 className="text-b"> {this.state.profile.ig}</h6>
                    </div>
                    </div>
                    <div className="side-profile">
                      <h6 className="profile-sub-title"> Line Id </h6>
                      <h6 className="text-b"> {this.state.profile.lineId}</h6>
                    </div>
                    <div className="side-profile">
                      <h6 className="profile-sub-title"> Email </h6>
                      <h6 className="text-b"> {this.state.profile.email}</h6>
                    </div>



                    {/* <FacebookShareButton
            url={window.location.href}
            quote='test'
            className="full-button"
          > */}
                    {/* <FacebookIcon size={32} round /> */}
                    <Button className="full-button fb" variant="primary" onClick={() => this.handleShow()}>Share this project</Button>{' '}
                    {/* </FacebookShareButton> */}

                  </Col>

                </Row>

              </div>










            </Col>
            <Col lg="3" className="hiddenBox"></Col>
            <Col lg="9" className="content-section" >
              <Row  style={{ display:'flow-root'}}>
                
                <Col lg="4" md="12" className="side-col " style={{ float:'right'}}>
                  
                  <div className="text-center">
                    <h1 className="theme-red project-name">{this.state.project.projectName}</h1>
                    <p className="none"> {this.state.project.detail1}</p>
                  </div>
                  <div>


                    {this.renderRandomProject()}
                  </div>







                </Col>

                <Col lg="8"  style={{ float:'right'}}>
                  <div className="gallery">
                    <ImageGallery
                      ref={i => this._imageGallery = i}
                      items={this.state.imagesRender}
                      lazyLoad={false}
                      onClick={this._onImageClick.bind(this)}
                      onImageLoad={this._onImageLoad}
                      onSlide={this._onSlide.bind(this)}
                      onPause={this._onPause.bind(this)}
                      onScreenChange={this._onScreenChange.bind(this)}
                      onPlay={this._onPlay.bind(this)}
                      infinite={this.state.infinite}
                      showBullets={this.state.showBullets}
                      showFullscreenButton={this.state.showFullscreenButton && this.state.showGalleryFullscreenButton}
                      showPlayButton={this.state.showPlayButton && this.state.showGalleryPlayButton}
                      showThumbnails={this.state.showThumbnails}
                      showIndex={this.state.showIndex}
                      showNav={this.state.showNav}
                      isRTL={this.state.isRTL}
                      thumbnailPosition={this.state.thumbnailPosition}
                      slideDuration={parseInt(this.state.slideDuration)}
                      slideInterval={parseInt(this.state.slideInterval)}
                      slideOnThumbnailOver={this.state.slideOnThumbnailOver}
                      additionalClass="app-image-gallery"
                    />
                    <br/>
                    <br/>
                    {this.state.project.detail2}
                  </div>
                </Col>
              </Row>
              {this.state.project.youtubeUrl ?
              <Row>
                <Col>
                <div className="youtube">
                  <YouTube
                    videoId={this.getYoutubeId(this.state.project.youtubeUrl)}
                  />
                  <br/>
                  <br/>
                  {this.state.project.detail3}
                  <br/> 
                  <br/>
                </div>
                </Col>
                </Row> : null}

              {this.renderImage()}






              <Row>

                <Col>
                  <Row className="renderRecommended" >

                    {this.renderRecommended()}
                  </Row>
                </Col>

              </Row>
            </Col>


          </Row>


          <Modal show={this.state.show} onHide={() => this.handleClose()} animation={false}>
            <Modal.Header closeButton className="text-center">
              <Modal.Title >Once upon a time</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Row>
                <Col lg="8" md="8" xs="8">
                  {this.state.project.postcardImage ?
                    <Image alt="test" src={require('../../../data/postcard/' + this.state.project.postcardImage)} className="postcardImg" style={{borderRadius: 15}} />
                    : null}
                </Col>
                <Col lg="4" md="4" xs="4">
                  <div className="side-menu">
                    <div className="side-sub-menu">


                      {/* <FacebookIcon size={32} round /> */}
                      <p className="save-Image-p" onClick={() => this.onClickSaveImage(this.state.project.postcardImage)}>  <Image alt="test" src={require('../../../data/subIcon/direct-download.png')} className="download-icon" />  save it let it be your </p>


                      <p >or</p>
                      <FacebookShareButton
                        url={window.location.href}
                        quote={this.state.project.projectName}
                        className="full-button"
                      >
                        {/* <FacebookIcon size={32} round /> */}
                        <Button className="full-button fb" variant="primary" >Share this project</Button>{' '}
                      </FacebookShareButton>
                    </div>
                  </div>
                </Col>
              </Row>

            </Modal.Body>
            {/* <Modal.Footer>
          <Button variant="secondary" onClick={() => this.handleClose()}>
            Close
          </Button>
          <Button variant="primary" onClick={() => this.handleClose()}>
            Save Changes
          </Button>
        </Modal.Footer> */}
          </Modal>

        </Container >
        </div>
</div>
      );
    } else {
      return (
        <Container >
          This page is not have
        </Container >
      );
    }

  }


}

const mapStateToProps = state => ({
  user: state.user
});

const mapDispatchToProps = dispatch => ({
  storeUserAccessToken: (
    accessToken
  ) => {
    dispatch({
      type: ActionUser.STORE_USER_ACCESS_TOKEN,
      accessToken: accessToken
    });
  },
  storeUserBalance: (
    balance
  ) => {
    dispatch({
      type: ActionUser.STORE_BALANCE,
      balance: balance
    });
  },
  storeServiceInfo: (
    id,
    image,
    name,
    roundId,
    service_endtime,
    service_starttime
  ) => {
    dispatch({
      type: ActionService.STORE_SERVICE_INFO,
      id: id,
      image: image,
      name: name,
      roundId: roundId,
      service_endtime: service_endtime,
      service_starttime: service_starttime
    });
  },
  setHardMode: () => {
    dispatch({
      type: ActionLottery.SET_HARD_MODE
    });
  },
  resetDataEzMode: () => {
    dispatch({
      type: ActionLottery.RESET_DATA_EZ_MODE
    });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(DetailsPage);
