import React, { Component } from "react";
import { Container, Row, Col, Alert, Card, CardDeck, Image } from 'react-bootstrap';
import Countdown from 'react-countdown';
import { Link } from "react-router-dom";
import * as Scroll from 'react-scroll';
import { Link as LinkScroll, Element, Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll'
// import { Slide } from 'react-slideshow-image';
import renderIf from 'render-if';
import Swal from "sweetalert2";
import { connect } from "react-redux";
import YouTube from 'react-youtube';
import ActionUser from "../../../actions/actionUser";
import ActionService from "../../../actions/actionService";
import ActionLottery from "../../../actions/actionLottery";
import categories from '../../../data/categories.js'
import subCategories from '../../../data/subCategories.js'
import images from '../../../data/images.js'
import projects from '../../../data/projects.js'
import './styles.css';
import ImageGallery from 'react-image-gallery';
class CategoryPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      categories: categories.categories,
      subCategories: subCategories.subCategories,
      catagoryId: '',
      catagoryName: '',
      listImage: [{
        original: `${require('../../../data/project/project1_1.png')}`,
        thumbnail: `${require('../../../data/project/project1_1.png')}`

      }],
      listImage2: [],
      imageset: true,
      showIndex: false,
      showBullets: false,
      infinite: true,
      showThumbnails: false,
      showFullscreenButton: false,
      showGalleryFullscreenButton: false,
      showPlayButton: false,
      showGalleryPlayButton: false,
      showNav: false,
      isRTL: false,
      slideDuration: 450,
      slideInterval: 5000,
      slideOnThumbnailOver: false,
      thumbnailPosition: 'bottom',
    };
  }

  componentDidMount() {
    this.setCategoryImageList();
    if (this.props.match.params.id) {
      this.selectCategory(this.props.match.params.id)
    }
  }
  async setCategoryImageList() {
    var catagoryId = [];
    categories.categories.forEach(element => {

      catagoryId.push(element.id);

    });

    var subCategories = {};

    catagoryId.forEach(cate => {
      var subCategorie = []
      this.state.subCategories.forEach(element => {
        if (element.categoryId === cate) {
          subCategorie.push(element.id)
        }
      });
      subCategories[cate] = subCategorie;

    });

    // console.log(typeof (subCategories));
    var aa = [];
    var limit = 4;
    var listimage = [];
    // console.log('projects.projects.length', projects.projects.length)
    // for (var index in subCategories) {
    //   var aaaa = [];
    //   // eslint-disable-next-line no-loop-func
    //   var i = 0;
    //   for (var j = 0; j < projects.projects.length; j++) {

    //     if (subCategories[index].includes(projects.projects[j].subCategoryId ? (projects.projects[j].subCategoryId.match(/\d+/) ? projects.projects[j].subCategoryId.match(/\d+/)[0] : null) : null)) {
    //       let titleImage = images.images.find(image => (image.projectId === projects.projects[j].id && image.isTitle === 'TRUE'));
    //       if (titleImage !== undefined) {
    //         if (i > limit) {
    //           break;
    //         } else {
    //           aaaa.push({
    //             original: `${require('../../../data/project/' + titleImage.image)}`,
    //             thumbnail: `${require('../../../data/project/' + titleImage.image)}`

    //           });
    //           i++;
    //         }

    //       }

    //     }
    //   }

    //   aa.push(aaaa);

    // }
    // console.log('subCategories', subCategories)
    var projectImCategory = [];
    // for (var index in subCategories) {
    //   if (subCategories[index].includes(projects.projects[j].subCategoryId ? (projects.projects[j].subCategoryId.match(/\d+/) ? projects.projects[j].subCategoryId.match(/\d+/)[0] : null) : null)) {
    //     projectImCategory
    //   }
    // }

    for (var j = 0; j < projects.projects.length; j++) {
      for (var index in subCategories) {
        if (subCategories[index].includes(projects.projects[j].subCategoryId ? (projects.projects[j].subCategoryId.match(/\d+/) ? projects.projects[j].subCategoryId.match(/\d+/)[0] : null) : null)) {
          if (projectImCategory[index] == undefined) {
            projectImCategory[index] = [];
          }
          projectImCategory[index].push(projects.projects[j].id);
        }
      }
    }
    // console.log('projectImCategory', projectImCategory);

    let listimage3 = []
    var randomProjectInCategory = [];
    for (var ii = 1; ii < projectImCategory.length; ii++) {
      let listsize = projectImCategory[ii].length;
      let randomArr = [];
      let randomArr2 = [];
      let aaaa =[];
      let i = 0;
      let maxsize = 3;
      // console.log('Listsize', listsize)
      if (listsize > 0) {


        while (i < maxsize) {
          // var r =Math.random() * max
          let r = Math.floor(Math.random() * listsize);

          if (!randomArr.includes(projectImCategory[ii][r])) {


            let titleImage = images.images.find(image => (image.projectId === projectImCategory[ii][r] && image.isTitle === 'TRUE'));
            // console.log("Home -> setCategoryImageList -> titleImage", titleImage)
            if (titleImage !== undefined) {
              aaaa.push({
                original: `${require('../../../data/project/' + titleImage.image)}`,
                thumbnail: `${require('../../../data/project/' + titleImage.image)}`
              });
      
          randomArr.push(projectImCategory[ii][r]);
          i++;
          } 
          // else {
          //     aaaa.push({
          //       original: `${require('../../../data/project/temp.png')}`,
          //       thumbnail: `${require('../../../data/project/temp.png')}`
          //     });
          //   }





            // randomArr.push(projectImCategory[ii][r]);
            // i++;

          }
          if (!randomArr2.includes(projectImCategory[ii][r])) {
            randomArr2.push(projectImCategory[ii][r]);

          }

          if (randomArr2.length === listsize) {
            i = maxsize;
          }

        }
      }
      listimage3.push(aaaa)
      randomProjectInCategory.push(randomArr);
    }

    // console.log('randomProjectInCategory',randomProjectInCategory)
    // var listimage3 = [];
    // for(var cate of randomProjectInCategory){
    //   var aaaa = [];
    //   cate.forEach(cate => {

    //     let titleImage = images.images.find(image => (image.projectId === cate && image.isTitle === 'TRUE'));
    //     if (titleImage !== undefined) {
        
          
    //         aaaa.push({
    //           original: `${require('../../../data/project/' + titleImage.image)}`,
    //           thumbnail: `${require('../../../data/project/' + titleImage.image)}`

    //         });
           
          

    //     }else{
    //       aaaa.push({
    //         original: `${require('../../../data/project/temp.png' )}`,
    //         thumbnail: `${require('../../../data/project/temp.png')}`
    //       });
    //     }

    //     // console.log(cate)
    //   })

    //   listimage3.push(aaaa);

    // }
    // console.log('listimage3', listimage3);

    await this.setState({
      listImage2: listimage3,
      imageset: false
    });

    // console.log('aa', aa);
    // console.log('listImage2', this.state.listImage2);

    // console.log('catagoryId' , catagoryId);

  }
  selectCategory = (id) => {
    // console.log("CategoryPage -> selectCategory -> id", id)
    // this.props.history.push('/category/'+id);
    let category = this.state.categories.find(element => (element.id === id));

    this.setState({
      catagoryId: id,
      catagoryName: category === undefined ? '' : category.name
    })
    scroller.scrollTo('subCategory', {
      duration: 800,
      delay: 0,
      smooth: 'easeInOutQuart'
    })
  }

  subCategorieStyle = () =>{
    if(this.state.catagoryId == '4' ){
      return '6';
    }
    else{
      return '4';
    }
  }
  renderSubCata = () => {
    let id = this.state.catagoryId;
    let sub = [];
    if (id !== '') {

      this.state.subCategories.forEach(element => {
        if (element.categoryId === id) {
          sub.push(element);
        }
      });
    }

    const listItem = sub.map((data, index) =>
      <Col lg={this.subCategorieStyle()} md={this.subCategorieStyle()} xs="6" key={index} className="sub-c">
        <Card body className="sub-category" key={index}>
          <Link
            to={{
              pathname: "/projects/" + data.id
            }}
          >
            <Image src={require('../../../assets/subIcon/'+ data.name.replace(/[\ \/ ]+/g, '') + '.png')}></Image>
            {/* <div className="sub-category-title" style={{ fontFamily: 'myriad-pro-condensed' }}>{data.name.replace(/[\ \/ ]+/g, '')}</div> */}
            {/* <div className="sub-category-title" style={{ fontFamily: 'myriad-pro-condensed' }}></div> */}
          </Link>

        </Card>
      </Col>
    );

    return (
      <>
        {listItem}
      </>
    );

  }

  categories = () => {
    if (this.state.imageset) {
      return null
    } else {


      const listItem = this.state.categories.map((data, index) =>

        <Card style={{ borderRadius: 10, boxShadow: (this.state.catagoryId === data.id ? '5px 10px #E9DDC9' : '') }} onClick={() => this.selectCategory(data.id)} key={index}>
          <ImageGallery
            ref={i => this.listImage = i}
            items={this.state.listImage2[index]}
            lazyLoad={false}
            onImageLoad={this._onImageLoad}
            onSlide={this._onSlide.bind(this)}
            onPause={this._onPause.bind(this)}
            onScreenChange={this._onScreenChange.bind(this)}
            onPlay={this._onPlay.bind(this)}
            infinite={this.state.infinite}
            showBullets={this.state.showBullets}
            showFullscreenButton={this.state.showFullscreenButton && this.state.showGalleryFullscreenButton}
            showPlayButton={this.state.showPlayButton && this.state.showGalleryPlayButton}
            showThumbnails={this.state.showThumbnails}
            showIndex={this.state.showIndex}
            showNav={this.state.showNav}
            isRTL={this.state.isRTL}
            thumbnailPosition={this.state.thumbnailPosition}
            slideDuration={parseInt(this.state.slideDuration)}
            slideInterval={parseInt(this.state.slideInterval)}
            slideOnThumbnailOver={this.state.slideOnThumbnailOver}
            additionalClass="app-image-gallery"
            autoPlay={true}


          />
          {/* <Card.Img fluid="true" variant="top" src={require('../../../data/project/project1_4.png')} className="wrapper" /> */}
          <div className="CatagoryText" style={{ cursor: 'pointer', fontFamily: 'myriad-pro-condensed' }}>{data.shortName}</div>

        </Card>


      );
      return (
        <>
          {listItem}
        </>
      )
    }
  }
  catagoryName = () => {
    if (this.state.catagoryName !== '') {
      return (
        <>
          <div className='d-flex flex-row-reverse' style={{ width: '100%', fontSize: 32, fontWeight: 'bold', fontStyle: 'italic', color: '#192F58' , marginBottom:'20px' , paddingRight:'50px'}}>
            <div style={{ textAlign: 'right' }}>
              <h1>{this.state.catagoryName.toUpperCase()}</h1>
              <div className='d-flex flex-row-reverse' style={{ width: '100%' }}>
                <div style={{ height: 5, backgroundColor: '#A51E25', width: '30%', marginBottom: 12 }} />
              </div>
            </div>
          </div>
        </>
      )
    } else {
      return null
    }
  }

  render() {

    return (
      <Container style={{ backgroundColor: 'white', height: '100%' }}>
        <div className="card-wrapper">
          {/* <Row className='justify-content-center align-items-center' style={{ height: 200 }}>
          </Row> */}
          <Row style={{ paddingTop: 100 }}>
            <CardDeck>
              {this.categories()}
            </CardDeck>
          </Row>
          <Row style={{ marginTop: 18 }} className={this.state.catagoryName !== ''?"subCategory catagoryName bg-cl":'subCategory catagoryName'}>
            {this.catagoryName()}
            {this.renderSubCata()}
          </Row>
          {/* <Element name="subCategory" className={this.state.catagoryName !== ''?"element bg-cl":'element'}> */}
            
          {/* </Element> */}
        </div>
      </Container >
    );
  }


  _onImageClick(event) {
    console.debug('clicked on image', event.target, 'at index', this._imageGallery.getCurrentIndex());
  }

  _onImageLoad(event) {
    console.debug('loaded image', event.target.src);
  }

  _onSlide(index) {
    // this._resetVideo();
    console.debug('slid to index', index);
  }

  _onPause(index) {
    console.debug('paused on index', index);
  }

  _onScreenChange(fullScreenElement) {
    console.debug('isFullScreen?', !!fullScreenElement);
  }

  _onPlay(index) {
    console.debug('playing from index', index);
  }

  _handleInputChange(state, event) {
    this.setState({ [state]: event.target.value });
  }

  _handleCheckboxChange(state, event) {
    this.setState({ [state]: event.target.checked });
  }

  _handleThumbnailPositionChange(event) {
    this.setState({ thumbnailPosition: event.target.value });
  }
}





export default connect()(CategoryPage);
