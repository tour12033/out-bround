import React, { Component } from "react";
import { Container, Row, Col, Alert } from 'react-bootstrap';
import Countdown from 'react-countdown';
import { Link } from "react-router-dom";
import { Modal } from 'react-bootstrap';
// import { Slide } from 'react-slideshow-image';
import renderIf from 'render-if';
import Swal from "sweetalert2";
import { connect } from "react-redux";

import ActionUser from "../../../actions/actionUser";
import ActionService from "../../../actions/actionService";
import ActionLottery from "../../../actions/actionLottery";

import './styles.css';

import projects from '../../../data/projects.js'
import images from '../../../data/images.js'
import profiles from '../../../data/profiles.js'
import subCategories from '../../../data/subCategories.js'
import categories from '../../../data/categories.js'


class FormalPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      dataYeeGee: [],
      isShowModal: false,
      isSelectValue: false,
      isShowAlert: false,
      textAlert: '',
      valueAll: 0,
      roundId: '',
      onlineUser: 0,
      username: '',
      selectLotto: 1
    };
  }

  async componentDidMount() {
    let data = await []
    let imagesList = await []
    let profile = null
    let subCategory = null
    let category = null
    await projects.projects.forEach(projectItem => {
      imagesList = []
      profile = null
      subCategory = null
      images.images.forEach(imageItem => {
        if (projectItem.id === imageItem.projectId) {
          imagesList.push(imageItem)
        }
      });
      profile = profiles.profiles.find(profileItem => (projectItem.profileId === profileItem.id));
      subCategory = subCategories.subCategories.find(subCategoryItem => (subCategoryItem.id === projectItem.subCategoryId));
      category = categories.categories.find(categoryItem => (categoryItem.id === subCategory.categoryId))
      data.push({
        id: projectItem.id,
        projectName: projectItem.projectName,
        detailTitle: projectItem.detailTitle,
        detailFull: projectItem.detailFull,
        latitude: projectItem.latitude,
        longitude: projectItem.longitude,
        profile: profile,
        subCategory: subCategory,
        images: imagesList,
        CategoryId: category.id,
        CategoryName: category.name
      })
    });
    // console.log(await data);
  }




  render() {
    return (
      <Container style={{ backgroundColor: 'red', height: '100vh' }}>
        <Row className='justify-content-center align-items-center' style={{ height: 500 }}>
          news
        </Row>
        <Row>
          <Col>1 of 2</Col>
          <Col>2 of 2</Col>
        </Row>
        <Row>
          <Col>1 of 3</Col>
          <Col>2 of 3</Col>
          <Col>3 of 3</Col>
        </Row>
      </Container >
    );
  }
}

const mapStateToProps = state => ({
  user: state.user
});

const mapDispatchToProps = dispatch => ({
  storeUserAccessToken: (
    accessToken
  ) => {
    dispatch({
      type: ActionUser.STORE_USER_ACCESS_TOKEN,
      accessToken: accessToken
    });
  },
  storeUserBalance: (
    balance
  ) => {
    dispatch({
      type: ActionUser.STORE_BALANCE,
      balance: balance
    });
  },
  storeServiceInfo: (
    id,
    image,
    name,
    roundId,
    service_endtime,
    service_starttime
  ) => {
    dispatch({
      type: ActionService.STORE_SERVICE_INFO,
      id: id,
      image: image,
      name: name,
      roundId: roundId,
      service_endtime: service_endtime,
      service_starttime: service_starttime
    });
  },
  setHardMode: () => {
    dispatch({
      type: ActionLottery.SET_HARD_MODE
    });
  },
  resetDataEzMode: () => {
    dispatch({
      type: ActionLottery.RESET_DATA_EZ_MODE
    });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(FormalPage);
