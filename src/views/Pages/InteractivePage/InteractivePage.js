import React, { Component } from "react";
import { Container, Row, Col, Alert } from 'react-bootstrap';
import { GoogleMap, Marker } from "react-google-maps"
import Countdown from 'react-countdown';
import { Link } from "react-router-dom";
import { Modal } from 'react-bootstrap';
// import { Slide } from 'react-slideshow-image';
import renderIf from 'render-if';
import Swal from "sweetalert2";
import { connect } from "react-redux";

import DemoApp from "./ReactGoogleMaps.js";
import ActionUser from "../../../actions/actionUser";
import ActionService from "../../../actions/actionService";
import ActionLottery from "../../../actions/actionLottery";

import './styles.css';

class InteractivePage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      dataYeeGee: [],
      isShowModal: false,
      isSelectValue: false,
      isShowAlert: false,
      textAlert: '',
      valueAll: 0,
      roundId: '',
      onlineUser: 0,
      username: '',
      selectLotto: 1
    };
  }

  async componentDidMount() {
    // await this.getAccessToken()
    // await this.getBalance()
    // await this.getOnlineUser()
    // this.props.resetDataEzMode()
    // this.props.setHardMode()
  }




  render() {
    // console.log('innerHeight',innerWidth)
    return (
      <div style={{ height: '100vh' }}>
        <DemoApp />
      </div >
    );
  }
}

const mapStateToProps = state => ({
  user: state.user
});

const mapDispatchToProps = dispatch => ({
  storeUserAccessToken: (
    accessToken
  ) => {
    dispatch({
      type: ActionUser.STORE_USER_ACCESS_TOKEN,
      accessToken: accessToken
    });
  },
  storeUserBalance: (
    balance
  ) => {
    dispatch({
      type: ActionUser.STORE_BALANCE,
      balance: balance
    });
  },
  storeServiceInfo: (
    id,
    image,
    name,
    roundId,
    service_endtime,
    service_starttime
  ) => {
    dispatch({
      type: ActionService.STORE_SERVICE_INFO,
      id: id,
      image: image,
      name: name,
      roundId: roundId,
      service_endtime: service_endtime,
      service_starttime: service_starttime
    });
  },
  setHardMode: () => {
    dispatch({
      type: ActionLottery.SET_HARD_MODE
    });
  },
  resetDataEzMode: () => {
    dispatch({
      type: ActionLottery.RESET_DATA_EZ_MODE
    });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(InteractivePage);
