import _ from "lodash";
import React, { Component } from "react";
import { Container, Row, Col, Alert, Form, Image } from 'react-bootstrap';
import { compose, withProps, withHandlers } from "recompose";
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
  InfoWindow
} from "react-google-maps";
import { MarkerClusterer } from "react-google-maps/lib/components/addons/MarkerClusterer";
import logo from "../../../assets/img/logo.png";

import projects from '../../../data/projects.js'
import images from '../../../data/images.js'
import sustainables from '../../../data/sustainables.js'
const mapStyles = require("./mapStyles.json")
// import GitHubForkRibbon from "react-github-fork-ribbon";
// import Header from "../../Header";
const enhance = _.identity;

const MyMapComponent = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyAFpvbzMt_dquFEWpevH45afBPi_o60Eb0&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%`, width: '100%' }} />,
    containerElement: <div style={{ height: `100%`, width: '100%' }} />,
    mapElement: <div style={{ height: `100%`, width: '100%' }} />,
  }),
  withScriptjs,
  withGoogleMap
)(props =>
  <GoogleMap
    defaultZoom={3}
    defaultCenter={{ lat: 25.0391667, lng: 121.525 }}
    defaultOptions={mapStyles}
  >
    <MarkerClusterer
      onClick={props.onMarkerClustererClick}
      averageCenter
      enableRetinaIcons
      gridSize={50}
    >
      {props.markers.map(marker => (
        <Marker
          key={marker.id}
          // icon={logo}
          position={{ lat: Number(marker.latitude), lng: Number(marker.longitude) }}
          onClick={() => { props.onMarkerClick(marker.id) }}
        >
          {props.showId === marker.id && <InfoWindow onCloseClick={props.onCloseInfo}>
            <div>
              <Image src={require('../../../data/project/' + marker.image)} height={200} width={'auto'} rounded />
              <div>{marker.projectName}</div>
              <div onClick={()=>{props.goProjectDetail(marker.id)}}>Detail</div>
            </div>
          </InfoWindow>}
        </Marker>

      ))}
    </MarkerClusterer>
  </GoogleMap>
)

class MyFancyComponent extends React.PureComponent {
  state = {
    filter: [],
    selectFilter: [],
    isMarkerShown: false,
    markers: [],
    showId: ''
  }

  componentDidMount() {
    let listProject = projects.projects
    let listImage = images.images
    listProject.forEach((itemProject, indexProject) => {
      listImage.forEach((itemImage, indexImage) => {
        if(itemImage.projectId === itemProject.id && itemImage.isTitle === "TRUE"){
          listProject[indexProject].image = itemImage.image
        }
      });
    });
    this.delayedShowMarker()
    this.setState({ 
      markers: projects.projects,
      filter: sustainables.sustainables
    });
    // const url = [
    //   // Length issue
    //   `https://gist.githubusercontent.com`,
    //   `/farrrr/dfda7dd7fccfec5474d3`,
    //   `/raw/758852bbc1979f6c4522ab4e92d1c92cba8fb0dc/data.json`
    // ].join("")

    // fetch(url)
    //   .then(res => res.json())
    //   .then(data => {
    //     this.setState({ markers: data.photos });
    //   });
  }

  onMarkerClick = (id) => {
    // console.log(id);
    this.setState({
      showId: id
    })
  };

  onCloseInfo = () => {
    this.setState({
      showId: ''
    })
  };

  delayedShowMarker = () => {
    setTimeout(() => {
      this.setState({ isMarkerShown: true })
    }, 3000)
  }

  goProjectDetail = (id) => {
    window.location.href = "/Details/" + id;
  }

  handleMarkerClick = () => {
    this.setState({ isMarkerShown: false })
    this.delayedShowMarker()
  }

  handleChangeCheckBox(evt, id) {
    let list = this.state.selectFilter
    if (evt.target.checked) {
      if (!list.includes(id)) {
        list.push(id)
      }
    } else {
      const index = list.indexOf(id);
      if (index > -1) {
        list.splice(index, 1);
      }
    }
    this.setState({
      selectFilter: list
    })
    this.getDataFilter()
    // this.setState({ checkboxChecked: evt.target.checked });
  }
  getDataFilter() {
    let listProject = []
    this.state.selectFilter.forEach(filter => {
      projects.projects.forEach(itemProject => {
        if (filter === itemProject.sustainableId) {
          listProject.push(itemProject)
        }
      });
    });
    this.setState({
      markers: listProject
    })
    // this.setState({ checkboxChecked: evt.target.checked });
  }


  renderFilter = () => {
    // const data = [{ "name": "test1" }, { "name": "test2" }];
    const listItems = this.state.filter.map((item) =>
      <div className='d-flex' style={{ flexDirection: 'row' }}>
        <Form.Check aria-label={item.name} onChange={(e) => this.handleChangeCheckBox(e, item.id)} />
        <span>{item.name}</span>
      </div>
    );

    return (
      <div>
        {listItems}
      </div>
    );
  }

  render() {
    return (
      <div className='d-flex row' style={{ height: '100vh', width: '100%', flexDirection: 'row' }}>
        {/* <Row style={{ height: '100vh', backgroundColor: 'red' }}> */}
        <Col xs={8} md={10} lg={10} style={{ paddingLeft: 0, paddingRight: 0 }}>
          <MyMapComponent
            isMarkerShown={this.state.isMarkerShown}
            onMarkerClick={this.onMarkerClick}
            goProjectDetail={this.goProjectDetail}
            onClose={this.onMarkerClick}
            onCloseInfo={this.onCloseInfo}
            showId={this.state.showId}
            markers={this.state.markers}
          />
        </Col>
        <Col xs={4} md={2} lg={2}>
          <div className='d-flex justify-content-center align-items-center' style={{ flexDirection: 'column' }}>
            <div style={{ fontSize: 24, marginBottom: 12 }}>Site Location</div>
            <div style={{ height: 5, backgroundColor: '#A51E25', width: '60%', marginBottom: 12 }} />
            <div className='d-flex' style={{ flexDirection: 'row' }}>
              <span>Filter</span>
            </div>
            {this.renderFilter()}
          </div>
        </Col>
        {/* </Row> */}
      </div>
    )
  }
}

export default enhance(MyFancyComponent);
