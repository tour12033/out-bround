import React, { Component } from "react";
import { Container, Row, Col, Alert, Image, CardDeck, Card, CardGroup } from 'react-bootstrap';
import { Link as LinkScroll, Element, Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll'
import Countdown from 'react-countdown';
import { Link } from "react-router-dom";
import { Modal } from 'react-bootstrap';
// import { Slide } from 'react-slideshow-image';
import renderIf from 'render-if';
import Swal from "sweetalert2";
import { connect } from "react-redux";
import ImageGallery from 'react-image-gallery';
import ActionUser from "../../../actions/actionUser";
import ActionService from "../../../actions/actionService";
import ActionLottery from "../../../actions/actionLottery";
import projects from '../../../data/projects.js'
import profiles from '../../../data/profiles.js'
import images from '../../../data/images.js'
import subCategories from '../../../data/subCategories.js'
import './styles.css';
const PREFIX_URL = 'https://raw.githubusercontent.com/xiaolin/react-image-gallery/master/static/';
class ProjectsPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      validId: false,
      subCategorie: null,
      projectList: []

    };
  }
  componentDidMount() {
    let id = this.props.match.params.id
    let subCategorie = subCategories.subCategories.find(subCategorieItem => (Number(subCategorieItem.id) === Number(id)))
    let project = this.getProjectsBySubId(id);
    this.setState({
      subCategorie: subCategorie,
      projectList: project
    })
  }

  getProjectsBySubId(id) {
    // console.log(id)
    var projectList = [];
    let image = null;
    let profile = null;
    projects.projects.forEach(projectItem => {
      image = null
      profile = null
      if (projectItem.subCategoryId && (Number(projectItem.subCategoryId.match(/\d+/)) === Number(id))) {
        image = images.images.find(imageItem => (imageItem.projectId === projectItem.id && imageItem.isTitle === 'TRUE'));
        profile = profiles.profiles.find(profileItem => (profileItem.id === projectItem.profileId));
        if (image && profile) {
          projectList.push({ id: projectItem.id, projectName: projectItem.projectName, image: image.image, profile: profile })
        }
      }
    });
    return projectList;
  }

  renderProjects = () => {
    const listItem = this.state.projectList.map((data, index) =>
      <Col lg="4" md="6" xs="12">
        <Card style={{ width: '100%', cursor: 'pointer' }} onClick={() => { window.location.href = "/Details/" + data.id }}>
          <Card.Img fluid variant="top" src={require('../../../data/project/' + data.image)} style={{ height: '18rem', objectFit: 'cover' }} />
          <Card.Body>
            <Card.Text style={{ textDecoration: 'none!important', color: '#192F58' }}>
              <div style={{fontStyle:'italic'}}>{data.projectName}</div>
              <small className="text-muted">{'Designed by ' + data.profile.name}</small>
            </Card.Text>
          </Card.Body>
          {/* <Card.Footer>
          <small className="text-muted">{'Designed by ' + data.profile.name}</small>
        </Card.Footer> */}
        </Card>
      </Col>
    );

    return (
      <Row>
        {listItem}
      </Row>
    )
  }

  render() {
    if (this.state.projectList.length > 0) {
      return (
        <Container style={{ backgroundColor: 'white', paddingBottom: '50px', fontFamily: 'myriad-pro-condensed' }}>
          <Row md={12}>
            <Col md={12}>
              <div className='d-flex justify-content-center align-items-center' style={{ flexDirection: 'column' }}>
                <span style={{ fontSize: 48, textAlign: 'center', width: '100%' }}>
                  {this.state.subCategorie.name + ' Project'}
                </span>
                {/* <div style={{ height: 5, backgroundColor: '#A51E25', width: '50%', marginBottom: 12 }} /> */}
              </div>
            </Col>
          </Row>
          <dev className='d-flex align-items-center' style={{ marginTop: 20 }}>
            {this.renderProjects()}
          </dev>
        </Container >
      );
    } else {
      return (
        <Container >
          Project not found
        </Container >
      );
    }

  }

}

const mapStateToProps = state => ({
  user: state.user
});

const mapDispatchToProps = dispatch => ({
  storeUserAccessToken: (
    accessToken
  ) => {
    dispatch({
      type: ActionUser.STORE_USER_ACCESS_TOKEN,
      accessToken: accessToken
    });
  },
  storeUserBalance: (
    balance
  ) => {
    dispatch({
      type: ActionUser.STORE_BALANCE,
      balance: balance
    });
  },
  storeServiceInfo: (
    id,
    image,
    name,
    roundId,
    service_endtime,
    service_starttime
  ) => {
    dispatch({
      type: ActionService.STORE_SERVICE_INFO,
      id: id,
      image: image,
      name: name,
      roundId: roundId,
      service_endtime: service_endtime,
      service_starttime: service_starttime
    });
  },
  setHardMode: () => {
    dispatch({
      type: ActionLottery.SET_HARD_MODE
    });
  },
  resetDataEzMode: () => {
    dispatch({
      type: ActionLottery.RESET_DATA_EZ_MODE
    });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(ProjectsPage);
