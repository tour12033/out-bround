export default class Role {
    static Admin = 'Admin'
    static RD = 'RD'
    static QC = 'QC'
    static Maketing = 'Maketing'
    static Production = 'Production'
    static Stock = 'Stock'
    static Accounting = 'Accounting'

}