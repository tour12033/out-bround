export default class Menu {
  static Role = { id: 1, name: "Role" };
  static Employee = { id: 2, name: "Employee" };
  static Customer = { id: 3, name: "Customer" };
  static RawMaterial = { id: 4, name: "RawMaterial" };
  static ProductionDevelopment = { id: 5, name: "ProductionDevelopment" };
  static FDARegistration = { id: 6, name: "FDARegistration" };
  static MasterFormula = { id: 7, name: "MasterFormula" };
  static BulkSpecification = { id: 8, name: "BulkSpecification" };
  static JobSheet = { id: 9, name: "JobSheet" };
  static PackingOrder = { id: 10, name: "PackingOrder" };
  static Stock = { id: 11, name: "Stock" };
  static Purchase = { id: 12, name: "Purchase" };
  static ProductProfile = { id: 13, name: "ProductProfile" };
  static Permision = { id: 14, name: "Permision" };
}
