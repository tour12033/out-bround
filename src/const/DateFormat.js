export function dateFormat (dateTime) {
    var now = new Date(dateTime);
    var thmonth = new Array ("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน", "กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
    return now.getDate()+" "+thmonth[now.getMonth()]+" "+(0+now.getFullYear()+543); 
};

export function dayMonthFormat (dateTime) {
    var now = new Date(dateTime);
    var thmonth = new Array ("ม.ค","ก.พ","มี.ค","เม.ย","พ.ค","มิ.ย", "ก.ค","ส.ค","ก.ย","ต.ค","พ.ย","ธ.ค");
    return now.getDate()+" "+thmonth[now.getMonth()]; 
};

export function timeFormat (dateTime) {
    var now = new Date(dateTime);
    return (now.getHours().toString().length > 1 ? now.getHours() : '0' + now.getHours()) + ':' + (now.getMinutes().toString().length > 1 ? now.getMinutes() : '0' + now.getMinutes()); 
};

export function diffDateTime (dt2, dt1) {
    var diff =(dt2.getTime() - dt1.getTime()) / 1000;
    var diffSec = Math.abs(Math.round(diff))

    let days = Math.floor(diffSec / (60 * 60 * 24));

    let hours = Math.floor((diffSec / (60 * 60)) - (days * 24));

    let divisor_for_minutes = diffSec % (60 * 60);
    let minutes = Math.floor(divisor_for_minutes / 60);

    let divisor_for_seconds = divisor_for_minutes % 60;
    let seconds = Math.ceil(divisor_for_seconds);

    let obj = {
      "d": days,
      "h": hours,
      "m": minutes,
      "s": seconds
    };
    let str =''
    if(obj.d > 0) str = str + obj.d + ' วัน '
    if(obj.h > 0) str = str + obj.h + ' ชั่วโมง '
    if(obj.m > 0) str = str + obj.m + ' นาที'
    return str; 
};