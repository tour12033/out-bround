import React, { Component } from "react";
import { connect } from 'react-redux'
import { Route, Switch } from "react-router-dom";
import { Navbar, Nav, Form, FormControl, Button } from 'react-bootstrap';
import "./App.scss";

import logo from "./assets/img/logo.png";
import facebook from "./assets/img/facebook.png";
import ig from "./assets/img/ig.png";
import aaulogo from "./assets/img/aaulogo.png"
import youtube from "./assets/img/youtube.png";
// const DefaultLayout = React.lazy(() => import("./containers/DefaultLayout"));

const loading = () => (
  // <div className="animated fadeIn pt-3 text-center">Loading...</div>
  <div className="animated fadeIn pt-3 text-center"></div>
);

class NavbarComponent extends Component {
  render() {
    return (
      <div>
        {/* <Navbar sticky="top" bg="light" variant="light">
          <Navbar.Brand href="/">
            <img
              alt=""
              src={coin}
              width="30"
              height="30"
              className="d-inline-block align-top"
            />
          </Navbar.Brand>
          <Nav className="mr-auto">
            <Nav.Link href="Formal">HOME</Nav.Link>
            <Nav.Link href="Formal">PROJECT</Nav.Link>
            <Nav.Link href="Formal">NEWS</Nav.Link>
            <Nav.Link href="Formal">CONTACT</Nav.Link>
            <Nav.Link href="Interactive">MAP</Nav.Link>
          </Nav>
        </Navbar> */}
        <Navbar fixed="top" collapseOnSelect expand="lg" variant="dark" style={{ backgroundColor: '#192F58' }}>
          <Navbar.Brand href="/">
            <div className='row'>
              <div>
                <img
                  alt=""
                  src={logo}
                  // width="30"
                  height="50"
                  className="d-inline-block align-top"
                />
              </div>
              <div>
                <Nav.Link href="/" className="d-flex align-items-center" style={{ color: '#E8DDC9', fontFamily: 'myriad-pro-condensed', fontSize: 12 }}><div><div>MONTFORT DEL ROSARIO SCHOOL</div><div>OF ARCHITECTURE AND DESIGN</div></div>  
                <img
                  alt=""
                  src={aaulogo}
                  // width="30"
                  height="50"
                  className="d-inline-block align-top"
                /></Nav.Link>
              </div>
            </div>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
              <Navbar.Brand href="/">
              </Navbar.Brand>
            </Nav>
            <Nav className="d-flex justify-content-between" style={{ width: '60%', fontFamily: 'myriad-pro-condensed' }}>
              <Nav.Link href="/" className="d-flex align-items-center" style={{ color: '#E8DDC9' }}>HOME</Nav.Link>
              <Nav.Link href="/category" className="d-flex align-items-center" style={{ color: '#E8DDC9' }}>PROJECT</Nav.Link>
              <Nav.Link href="/About" className="d-flex align-items-center" style={{ color: '#E8DDC9' }}>ABOUT</Nav.Link>
              <Nav.Link href="/contact" className="d-flex align-items-center" style={{ color: '#E8DDC9' }}>CONTACT</Nav.Link>
              {/* <Nav.Link href="/Interactive" className="d-flex align-items-center" style={{ color: '#E8DDC9' }}>MAP</Nav.Link> */}
              <div className="d-flex row">
                <Nav.Link target="_blank" href="https://facebook.com/Thesisexhibition2020/?tsid=0.33230680231473797&source=result" style={{ color: '#E8DDC9', marginRight: 12 }}>
                  <img
                    alt=""
                    src={facebook}
                    // width="30"
                    height="40"
                    className="d-inline-block align-top"
                  />
                </Nav.Link>
                <Nav.Link target="_blank" href="https://www.instagram.com/outbound_2020/" style={{ color: '#E8DDC9', marginRight: 12 }}>
                  <img
                    alt=""
                    src={ig}
                    // width="30"
                    height="40"
                    className="d-inline-block align-top"
                  />
                </Nav.Link>
                <Nav.Link target="_blank" href="https://www.youtube.com/channel/UCOf32iPFo2J2qZnW-ihL6YQ" style={{ color: '#E8DDC9' }}>
                  <img
                    alt=""
                    src={youtube}
                    // width="30"
                    height="40"
                    className="d-inline-block align-top"
                  />
                </Nav.Link>
              </div>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        {/* {this.props.loading.isLoading ? (
          <LoadingPage />
        ) : null} */}
      </div>
    );
  }
}

// export default App;
const mapStateToProps = state => ({
  user: state.user,
  loading: state.loading
});

const mapDispatchToProps = dispatch => ({
  //   storeUserInfo: (firstName, lastName, phone, email, citizenId, status, token, permisions) => {
  //     dispatch({ 
  //       type: ActionUser.STORE_USER_INFO,
  //       firstName: firstName,
  //       lastName: lastName,
  //       phone: phone,
  //       email: email,
  //       citizenId: citizenId,
  //       status: status,
  //       token: token,
  //       permisions: permisions
  //   })
  // }
});
export default connect(mapStateToProps, mapDispatchToProps)(NavbarComponent);
