export default class ActionCount {
    static STORE_USER_INFO = 'STORE_USER_INFO'
    static STORE_USER_TOKEN = 'STORE_USER_TOKEN'
    static STORE_USER_ACCESS_TOKEN = 'STORE_USER_ACCESS_TOKEN'
    static SET_LOGIN = 'SET_LOGIN'
    static RESET_LOGIN = 'RESET_LOGIN'
    static STORE_BALANCE = 'STORE_BALANCE'

}