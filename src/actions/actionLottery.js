export default class ActionLottery {
    static SET_EZ_MODE = 'SET_EZ_MODE'
    static SET_HARD_MODE = 'SET_HARD_MODE'
    static STORE_DATA_EZ_MODE = 'STORE_DATA_EZ_MODE'
    static RESET_DATA_EZ_MODE = 'RESET_DATA_EZ_MODE'
}