// import Menu from "./const/Menu";
export default function getMenu() {
  let menus = [];
    menus.push({
      name: "หน้าหลัก",
      url: "/",
      // icon: "icon-control-pause"
    });
    menus.push({
      name: "ตรวจหวย",
      url: "/CheckLottery",
      // icon: "icon-user"
    });
    menus.push({
      name: "ประวัติการซื้อ",
      url: "/History",
      // icon: "icon-user"
    });
  return {
    items: menus
  };
}
