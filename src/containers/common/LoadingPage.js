import React from "react";
// import LoadingPic from "../images/loading.gif";
import { SyncLoader } from "react-spinners";
import { css } from "@emotion/core";

const LoadingPage = () => {
  return (
    <div
      style={{
        position: 'absolute',
        height: '100%',
        width: '100%',
        top: 0,
        backgroundColor: 'white',
        zIndex: 10000
      }}
    >
      <div
        style={{
          position: 'absolute', left: '50%', top: '50%',
          transform: 'translate(-50%, -50%)'
        }}
      >
        <SyncLoader
          // css={override}
          size={15}
          //size={"150px"} this also works
          color={"#2980B9"}
          loading={true}
        />
      </div>
    </div>
  );
};

export default LoadingPage;
