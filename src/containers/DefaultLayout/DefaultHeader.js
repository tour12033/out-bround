import React, { Component, View } from "react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import {
  UncontrolledDropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Nav,
  NavItem
} from "reactstrap";
import PropTypes from "prop-types";
import {
  AppNavbarBrand,
  AppSidebarToggler
} from "@coreui/react";
// import logo from "../../assets/img/brand/logo.svg";
import logo from "../../assets/img/brand/logo2.png";
import coin from "../../assets/img/coin.png";
// import sygnet from "../../assets/img/brand/sygnet.svg";

const propTypes = {
  children: PropTypes.node
};

const defaultProps = {};

class DefaultHeader extends Component {
  render() {
    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile/>
        {/* <AppNavbarBrand
          full={{ src: logo, width: 89, height: 40, alt: "CoreUI Logo" }}
          // minimized={{ src: logo, width: 30, height: 30, alt: "CoreUI Logo" }}
        /> */}
        <AppSidebarToggler className="d-md-down-none" display="lg" />

        {/* <Nav className="d-md-down-none" navbar>
          <NavItem className="px-3">
            <NavLink to="/dashboard" className="nav-link" >Dashboard</NavLink>
          </NavItem>
          <NavItem className="px-3">
            <Link to="/users" className="nav-link">Users</Link>
          </NavItem>
          <NavItem className="px-3">
            <NavLink to="#" className="nav-link">Settings</NavLink>
          </NavItem>
        </Nav> */}
        
        <Nav className="ml-auto" navbar>
          {/* <NavItem className="d-md-down-none">
            <NavLink to="#" className="nav-link"><i className="icon-bell"></i><Badge pill color="danger">5</Badge></NavLink>
          </NavItem>
          <NavItem className="d-md-down-none">
            <NavLink to="#" className="nav-link"><i className="icon-list"></i></NavLink>
          </NavItem>
          <NavItem className="d-md-down-none">
            <NavLink to="#" className="nav-link"><i className="icon-location-pin"></i></NavLink>
          </NavItem> */}
          <NavItem className="d-md-down-none">
            <NavLink to="#" className="nav-link"></NavLink>
          </NavItem>
          {/* <UncontrolledDropdown style={{marginRight: 10, color: 'white'}}>
          <img alt='coin' style={{ height: 20, width: 20, marginRight: 5 }} src={coin}></img>
            1000000
          </UncontrolledDropdown> */}
          {/* <UncontrolledDropdown nav direction="down">
            <DropdownToggle nav>
              <label style={{ paddingRight: 50 }}>
                {" "}
                {this.props.user.firstName +
                  " " +
                  this.props.user.lastName}{" "}
                <i className="fa fa-sort-desc"></i>{" "}
              </label>
              <img
                src={logo}
                className="img-avatar"
                alt="admin@bootstrapmaster.com"
              />
              <i className="cui-chevron-bottom icons font-2xl d-block mt-4"></i>
            </DropdownToggle>
            
            <DropdownMenu right>
              <DropdownItem header tag="div" className="text-center">
                <strong>Account</strong>
              </DropdownItem>
              <DropdownItem>
                <i className="fa fa-bell-o"></i> Updates
                <Badge color="info">42</Badge>
              </DropdownItem>
              <DropdownItem>
                <i className="fa fa-envelope-o"></i> Messages
                <Badge color="success">42</Badge>
              </DropdownItem>
              <DropdownItem>
                <i className="fa fa-tasks"></i> Tasks
                <Badge color="danger">42</Badge>
              </DropdownItem>
              <DropdownItem>
                <i className="fa fa-comments"></i> Comments
                <Badge color="warning">42</Badge>
              </DropdownItem>
              <DropdownItem header tag="div" className="text-center">
                <strong>Settings</strong>
              </DropdownItem>
              <DropdownItem onClick={e => this.props.onPersonal(e)}>
                <i className="fa fa-user"></i> ข้อมูลส่วนตัว
              </DropdownItem>
              <DropdownItem onClick={e => this.props.onChangePassword(e)}>
                <i className="fa fa-key"></i> เปลี่ยนรหัสผ่าน
              </DropdownItem>
              <DropdownItem>
                <i className="fa fa-wrench"></i> Settings
              </DropdownItem>
              <DropdownItem>
                <i className="fa fa-usd"></i> Payments
                <Badge color="secondary">42</Badge>
              </DropdownItem>
              <DropdownItem>
                <i className="fa fa-file"></i> Projects
                <Badge color="primary">42</Badge>
              </DropdownItem>
              <DropdownItem divider />
              <DropdownItem>
                <i className="fa fa-shield"></i> Lock Account
              </DropdownItem>
              <DropdownItem onClick={e => this.props.onLogout(e)}>
                <i className="fa fa-lock"></i> ออกจากระบบ
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown> */}
        </Nav>
        {/* <AppAsideToggler className="d-md-down-none" /> */}
        {/* <AppAsideToggler className="d-lg-none" mobile /> */}
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

const mapStateToProps = state => ({
  user: state.user
});

const mapDispatchToProps = dispatch => ({});
export default connect(mapStateToProps, mapDispatchToProps)(DefaultHeader);
