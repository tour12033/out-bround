import React, { Component, Suspense } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import * as router from "react-router-dom";
import { connect } from "react-redux";
import { Container } from "reactstrap";
// import Role from "../../const/Role";

import {
  AppAside,
  AppFooter,
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppBreadcrumb2 as AppBreadcrumb,
  AppSidebarNav2 as AppSidebarNav
} from "@coreui/react";
// sidebar nav config
import getMenu from "../../_nav";
// routes config
import routes from "../../routes";


import ActionUser from "../../actions/actionUser";

const DefaultAside = React.lazy(() => import("./DefaultAside"));
const DefaultFooter = React.lazy(() => import("./DefaultFooter"));
const DefaultHeader = React.lazy(() => import("./DefaultHeader"));

class DefaultLayout extends Component {
  // loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>
  loading = () => <div className="animated fadeIn pt-1 text-center"></div>;

  signOut(e) {
    e.preventDefault();
    this.props.logoutUser()
    this.props.history.push("/login");
  }

  personalOnclick(e) {
    e.preventDefault();
    this.props.history.push("/pages/Personal/PersonalInfo");
  }

  changePasswordOnClick(e) {
    e.preventDefault();
    this.props.history.push("/pages/ChangePassword/ChangePassword");
  }

  render() {
    return (
      <div className="app">
        <AppHeader fixed style={{ backgroundColor: '#26262C', border: 0 }}>
          <Suspense fallback={this.loading()}>
            <DefaultHeader
            // onPersonal={e => this.personalOnclick(e)}
            // onLogout={e => this.signOut(e)}
            // onChangePassword={e => this.changePasswordOnClick(e)}
            />
          </Suspense>
        </AppHeader>
        <div className="app-body">
          <AppSidebar fixed display="lg" style={{ backgroundColor: '#151515' }}>
            {/* <AppSidebarHeader />
            <AppSidebarForm /> */}
            <Suspense>
              <AppSidebarNav
                navConfig={getMenu()}
                {...this.props}
                router={router}
              />
            </Suspense>
            {/* <AppSidebarFooter />
            <AppSidebarMinimizer /> */}
          </AppSidebar>
          <main className="main">
            {/* <AppBreadcrumb appRoutes={routes} router={router} /> */}
            <div style={{ backgroundColor: 'black' }}>
              <Suspense fallback={this.loading()}>
                <Switch>
                  {routes.map((route, idx) => {
                    return route.component ? (
                      <Route
                        key={idx}
                        path={route.path}
                        exact={route.exact}
                        name={route.name}
                        render={props => <route.component {...props} />}
                      />
                    ) : null;
                  })}
                  <Redirect from="/" to="/dashboard" />
                </Switch>
              </Suspense>
            </div>
          </main>
          <AppAside fixed>
            <Suspense fallback={this.loading()}>
              <DefaultAside />
            </Suspense>
          </AppAside>
        </div>
        {/* <AppFooter>
          <Suspense fallback={this.loading()}>
            <DefaultFooter />
          </Suspense>
        </AppFooter> */}
      </div>
    );
  }
}
const mapStateToProps = state => ({
  user: state.user
});

const mapDispatchToProps = dispatch => ({
  logoutUser: () => {
    dispatch({
      type: ActionUser.RESET_LOGIN
    });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(DefaultLayout);
